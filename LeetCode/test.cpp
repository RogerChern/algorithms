
//  test.cpp
//  LeetCode
//
//  Created by RogerChen on 3/12/15.
//  Copyright (c) 2015 RogerChen. All rights reserved.
//

#include "test.h"

void test() {
    test_minCut();
    printf("tests passed!");
}

void test_all() {
     test_ladderLength();
     test_surroundedRegion();
     test_getPermutation();
     test_permuteUnique();
     test_maximumGap();
     test_NFA();
     test_strStr();
     test_myPow();
     test_double();
     test_maxPoints();
     test_evalRPN();
     test_sortList();
     test_isSymmetric();
     test_buildTree();
     test_combinationSum();
     test_combinationSum2();
     test_addBinary();
     test_countAndSay();
     test_rob();
     test_numIslands();
     test_solveSudoku();
     test_isValidSudoku();
     // test_zigzagLevelOrder();
     test_maxPathSum();
     test_isBalanced();
     test_reverseBits();
     // test_rotate();
     test_isDiff();
     test_grayCode();
     test_sortColor();
     test_simplifyPath();
     test_setZeroes();
     test_searchMatrix();
     // test_nextPermutation();
     test_firstMissingPositive();
     test_calculateMinimumHP();
     test_levelOrderBottom();
     test_compareVersion();
     test_split();
     test_stringToInteger();
     test_levelOrder();
     test_peakElement();
     test_majorityElement();
     test_BSTIterator();
     test_largestNumber();
     test_largestNumber_intToString();
     test_largestNumber_compare();
     test_findRepeatedDnaSequences();
     test_trailingZeroes();
     test_convertToTitle();
     test_longestValidParentheses();
     test_hammingWeight();
     test_titleToNumber();
     test_wordSearch();
     test_combine();
     test_cloneGraph();
}

void test_minCut() {
    assert(minCut("") == 0);
    assert(minCut("a") == 0);
    assert(minCut("aa") == 0);
    assert(minCut("ab") == 1);
    assert(minCut("aaa") == 0);
    assert(minCut("aab") == 1);
    assert(minCut("abc") == 2);
    assert(minCut("cabababcbc") == 3);
}

void test_largestDivisibleSubset() {
    vector<int> test1 = {12, 8, 6, 3, 2};
    vector<int> test2 = {1, 2, 4, 8};
    vector<int> test3 = {1, 2, 3};
    vector<int> test4 = {1};
    vector<int> test5 = {3, 4};
    vector<int> test6 = {1, 2, 4, 8, 9, 72};
    vector<int> test7 = {1, 2, 3, 4, 8, 9, 18, 36, 72};
    vector<int> result1 = largestDivisibleSubset(test1);
    vector<int> result2 = largestDivisibleSubset(test2);
    vector<int> result3 = largestDivisibleSubset(test3);
    vector<int> result4 = largestDivisibleSubset(test4);
    vector<int> result5 = largestDivisibleSubset(test5);
    vector<int> result6 = largestDivisibleSubset(test6);
    vector<int> result7 = largestDivisibleSubset(test7);

    assert(test_largestDivisibleSubset_check(result1) && result1.size() == 3);
    assert(test_largestDivisibleSubset_check(result2) && result2.size() == 4);
    assert(test_largestDivisibleSubset_check(result3) && result3.size() == 2);
    assert(test_largestDivisibleSubset_check(result4) && result4.size() == 1);
    assert(test_largestDivisibleSubset_check(result5) && result5.size() == 1);
    assert(test_largestDivisibleSubset_check(result6) && result6.size() == 5);
    assert(test_largestDivisibleSubset_check(result7) && result7.size() == 6);
}

bool test_largestDivisibleSubset_check(vector<int> &result) {
    sort(result.begin(), result.end());
    bool ok = true;
    for (int i = 0; i < result.size()-1; ++i) {
        ok &= (result[i+1] % result[i]) == 0;
    }
    return ok;
}

void test_numDecodings() {
    assert(2 == numDecodings("611"));
    assert(0 == numDecodings(""));
    assert(0 == numDecodings("30"));
    assert(0 == numDecodings("0"));
    assert(1 == numDecodings("310"));
    assert(589824 == numDecodings("4757562545844617494555774581341211511296816786586787755257741178599337186486723247528324612117156948"));
}

void test_fractionToDecimal() {
    assert("0.(3)" == fractionToDecimal(1, 3));
    assert("0.1(6)" == fractionToDecimal(1, 6));
    assert("1" == fractionToDecimal(-10, -10));
    assert("-1" == fractionToDecimal(-1, 1));
    assert("-1" == fractionToDecimal(1, -1));
    assert("-6.25" == fractionToDecimal(-50, 8));
    assert("-6.25" == fractionToDecimal(50, -8));
    assert("-0.16" == fractionToDecimal(-8, 50));
}

void test_ladderLength() {
    unordered_set<string> dict1 = {"hot","dot","dog","lot","log"};
    assert(5 == ladderLength("hit", "cog", dict1));
}

void test_surroundedRegion() {
    vector<vector<char>> test1 = {
        {'X', 'X', 'X', 'X'},
        {'X', 'O', 'O', 'X'},
        {'X', 'X', 'O', 'X'},
        {'X', 'O', 'X', 'X'}
    };
    vector<vector<char>> test2 = {
        {'O', 'O', 'O'},
        {'O', 'O', 'O'},
        {'O', 'O', 'O'}
    };
    vector<vector<char>> test3 = {
        {'O', 'O', 'O', 'O', 'O'},
        {'O', 'X', 'X', 'X', 'O'},
        {'O', 'X', 'O', 'X', 'O'},
        {'O', 'X', 'X', 'X', 'O'},
        {'O', 'O', 'O', 'O', 'O'}
    };
    // note the stack overflow error when using dfs
    vector<vector<char>> test4(5, vector<char>(5, 'O'));
    vector<vector<char>> result1 = {
        {'X', 'X', 'X', 'X'},
        {'X', 'X', 'X', 'X'},
        {'X', 'X', 'X', 'X'},
        {'X', 'O', 'X', 'X'}
    };
    vector<vector<char>> result2 = {
        {'O', 'O', 'O'},
        {'O', 'O', 'O'},
        {'O', 'O', 'O'}
    };
    vector<vector<char>> result3 = {
        {'O', 'O', 'O', 'O', 'O'},
        {'O', 'X', 'X', 'X', 'O'},
        {'O', 'X', 'X', 'X', 'O'},
        {'O', 'X', 'X', 'X', 'O'},
        {'O', 'O', 'O', 'O', 'O'}
    };
    vector<vector<char>> result4(5, vector<char>(5, 'O'));
    
    surroundedRegion(test1);
    surroundedRegion(test2);
    surroundedRegion(test3);
    surroundedRegion(test4);
    
    assert(test1 == result1);
    assert(test2 == result2);
    assert(test3 == result3);
    assert(test4 == result4);
}

void test_getPermutation() {
    assert("123" == getPermutation(3, 1));
    assert("132" == getPermutation(3, 2));
    assert("213" == getPermutation(3, 3));
    assert("231" == getPermutation(3, 4));
    assert("312" == getPermutation(3, 5));
    assert("321" == getPermutation(3, 6));
    assert("4312" == getPermutation(4, 23));
    assert("54312" == getPermutation(5, 119));
}

void test_permuteUnique() {
    vector<int> input1;
    vector<int> input2 = { 1 };
    vector<int> input3 = { 1, 2, 3 };
    vector<int> input4 = { 1, 1, 2 };
    vector<int> input5 = { 1, 1 };
    vector<vector<int>> test1;
    vector<vector<int>> test2 = { { 1 } };
    vector<vector<int>> test3 = { {1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1} };
    vector<vector<int>> test4 = { {1, 1, 2}, {1, 2, 1}, {2, 1, 1} };
    vector<vector<int>> test5 = { {1, 1} };

    assert(test1 == permuteUnique(input1));
    assert(test2 == permuteUnique(input2));
    assert(test3 == permuteUnique(input3));
    assert(test4 == permuteUnique(input4));
    assert(test5 == permuteUnique(input5));
}

void test_maximumGap() {
    mt19937_64 g(1994);
    vector<int> test1 = { 1, 2, 3, 4, 6 };
    vector<int> test2 = { 2, 999 };
    vector<int> test3 = { 2, 999, 999 };
    vector<int> test4 = { 1, 3, 5 };
    vector<int> test5;
    vector<int> test6 = { 1 };

    shuffle(test1.begin(), test1.end(), g);
    shuffle(test2.begin(), test2.end(), g);
    shuffle(test3.begin(), test3.end(), g);
    shuffle(test4.begin(), test4.end(), g);

    assert(2 == maximumGap(test1));
    assert(997 == maximumGap(test2));
    assert(997 == maximumGap(test3));
    assert(2 == maximumGap(test4));
    assert(0 == maximumGap(test5));
    assert(0 == maximumGap(test6));
}

void test_NFA() {
    NFA test1("A*");
    assert(test1.recognize("AA"));
    assert(test1.recognize("A"));
    assert(test1.recognize(""));

    NFA test2("(AB|AC)*D");
    assert(test2.recognize("D"));
    assert(test2.recognize("ABD"));
    assert(test2.recognize("ABABD"));
    assert(test2.recognize("ACD"));
    assert(test2.recognize("ACACD"));

    NFA test3("(A*|B)D");
    assert(test3.recognize("D"));
    assert(test3.recognize("BD"));
    assert(test3.recognize("AD"));
    assert(test3.recognize("AAD"));
    assert(test3.recognize("ABD") == false);

    NFA test4(".*");
    assert(test4.recognize("AJFLDJKLKFJSDKLFJDLSFKLJFDL"));

    NFA test5(".");
    assert(test5.recognize("") == false);
    assert(test5.recognize("A"));
    assert(test5.recognize("BD") == false);

    NFA test6("a");
    assert(test6.recognize("aa") == false);

    NFA test7("c*a*b*");
    assert(test7.recognize("aab"));
}

void test_strStr() {
    assert(strStr("aaa", "aaaa") == -1);
    assert(strStr("mississippi", "issip") == 4);
}

void test_myPow() {
    double x, y;
    for (int i = -300; i < 300; ++i) {
        x = myPow(10, i);
        y = pow(10, i);
        assert(fabs(x-y)/y < 10e-10);
    }
}

void test_double() {
    assert(1.0/2.0 == 2.0/4.0);
    assert(4.0/5.0 == 400.0/500.0);
    assert(412.0/522.0 == 824.0/1044.0);
}

void test_maxPoints() {
    vector<Point> points1;
    points1.emplace_back(1, 2);
    points1.emplace_back(2, 3);
    points1.emplace_back(3, 3);
    points1.emplace_back(2, 4);
    points1.emplace_back(3, 6);
    
    vector<Point> points2;
    points2.emplace_back(0, 0);
    
    vector<Point> points3;
    points3.emplace_back(0, 0);
    points3.emplace_back(0, 0);
    
    vector<Point> points4;
    points4.emplace_back(0, 0);
    points4.emplace_back(1, 1);
    points4.emplace_back(1, -1);
    
    vector<Point> points5;
    points5.emplace_back(4, 0);
    points5.emplace_back(4, -1);
    points5.emplace_back(4, 5);
    
    vector<Point> points6 = {{-230,324},{-291,141},{34,-2},{80,22},{-28,-134},{40,-23},{-72,-149},{0,-17},{32,-32},{-207,288},{7,32},{-5,0},{-161,216},{-48,-122},{-3,39},{-40,-113},{115,-216},{-112,-464},{-72,-149},{-32,-104},{12,42},{-22,19},{-6,-21},{-48,-122},{161,-288},{16,11},{39,23},{39,30},{873,-111}};
    
    vector<Point> points7 = {{0, 0}, {1, 1}, {0, 0}};
    
    assert(3 == maxPoints(points1));
    assert(1 == maxPoints(points2));
    assert(2 == maxPoints(points3));
    assert(2 == maxPoints(points4));
    assert(3 == maxPoints(points5));
    assert(9 == maxPoints(points6));
    assert(3 == maxPoints(points7));
}

void test_evalRPN() {
    vector<string> test1 = { "2", "1", "+", "3", "*" };
    vector<string> test2 = { "4", "13", "5", "/", "+" };
    vector<string> test3 = { "3", "-4", "+" };

    assert(9 == evalRPN(test1));
    assert(6 == evalRPN(test2));
    assert(-1 == evalRPN(test3));
}

bool LinkListEQ(ListNode *lhs, ListNode *rhs) {
    while (lhs != nullptr && rhs != nullptr) {
        if (lhs->val != rhs->val)
            return false;
        lhs = lhs->next;
        rhs = rhs->next;
    }
    
    if (lhs == nullptr && rhs == nullptr)
        return true;
    return false;
}

void test_sortList() {
    ListNode *test1 = new ListNode(12);
    test1->next = new ListNode(7);
    test1->next->next = new ListNode(13);
    ListNode *result1 = new ListNode(7);
    result1->next = new ListNode(12);
    result1->next->next = new ListNode(13);
    
    ListNode *ret1 = sortList(test1);
    assert(LinkListEQ(result1, ret1));
}

void test_isSymmetric() {
    TreeNode *test1 = new TreeNode(1);
    TreeNode *test2 = new TreeNode(1);
    test2->left = new TreeNode(2);
    test2->right = new TreeNode(2);
    TreeNode *test3 = new TreeNode(1);
    test3->left = new TreeNode(2);
    test3->right = new TreeNode(3);
    TreeNode *test4 = new TreeNode(1);
    test4->left = new TreeNode(2);
    
    assert(true == isSymmetric(nullptr));
    assert(true == isSymmetric(test1));
    assert(true == isSymmetric(test2));
    assert(false == isSymmetric(test3));
    assert(false == isSymmetric(test4));
}

void test_buildTree() {
    vector<int> postorder1 = { 1, 5, 4, 8, 9, 7 };
    vector<int> inorder1 = { 1, 4, 5, 7, 8, 9 };
    // auto result1 = buildTree(inorder1, postorder1);
    
    vector<int> postorder2 = { 3, 2, 4, 1};
    vector<int> inorder2 = { 1, 2, 3, 4 };
    // auto result2 = buildTree(inorder2, postorder2);
    
    vector<int> postorder3 = { 4, 3, 5, 2, 1 };
    vector<int> inorder3 = { 1, 2, 3, 4, 5 };
    // auto result3 = buildTree(inorder3, postorder3);
    
}

void test_combinationSum() {
    vector<int> test1 = { 2, 3, 6, 7 };
    vector<vector<int>> result1 = { { 2, 2, 3 }, { 7 } };
    auto ret1 = combinationSum(test1, 7);
    sort(ret1.begin(), ret1.end());
    
    assert(result1 == ret1);
}

void test_combinationSum2() {
    vector<int> test1 = { 10, 1, 2, 7, 6, 1, 5 };
    vector<vector<int>> result1 = { { 1, 7 }, { 1, 2, 5 }, { 2, 6 }, { 1, 1, 6 } };
    auto ret1 = combinationSum2(test1, 8);
    sort(ret1.begin(), ret1.end());
    sort(result1.begin(), result1.end());
    
    assert(result1 == ret1);
}

void test_addBinary() {
    assert(string("111") == addBinary("110", "1"));
    assert(string("0") == addBinary("0", "0"));
}

void test_countAndSay() {
    assert(string("1") == countAndSay(1));
    assert(string("11") == countAndSay(2));
    assert(string("21") == countAndSay(3));
    assert(string("1211") == countAndSay(4));
    assert(string("111221") == countAndSay(5));
    assert(string("312211") == countAndSay(6));
    assert(string("13112221") == countAndSay(7));
    assert(string("1113213211") == countAndSay(8));
}

void test_rob() {
    vector<int> test1 = { 1, 2, 3 };
    vector<int> test2 = { 1, 2, 3, 4, 5, 6 };
    vector<int> test3 = { 1 };
    vector<int> test4 = { 1, 2 };
    vector<int> test5 = {};
    assert(4 == rob(test1));
    assert(12 == rob(test2));
    assert(1 == rob(test3));
    assert(2 == rob(test4));
    assert(0 == rob(test5));
}

void test_numIslands() {
    vector<vector<char>> test1 = {
            { '1', '1', '1', '1', '0' },
            { '1', '1', '0', '1', '0' },
            { '1', '1', '0', '0', '0' },
            { '0', '0', '0', '0', '0' }
    };

    vector<vector<char>> test2 = {
            { '1', '1', '0', '0', '0' },
            { '1', '1', '0', '0', '0' },
            { '0', '0', '1', '0', '0' },
            { '0', '0', '0', '1', '1' }
    };

    vector<vector<char>> test3 = {
            { '0', '0', '0', '0', '0' },
            { '0', '0', '0', '0', '0' },
            { '0', '0', '0', '0', '0' },
            { '0', '0', '0', '0', '0' }
    };

    vector<vector<char>> test4 = {
            { '1', '1', '1', '1', '1' },
            { '1', '1', '1', '1', '1' },
            { '1', '1', '1', '1', '1' },
            { '1', '1', '1', '1', '1' }
    };

    vector<vector<char>> test5 = {
            { '1' }
    };

    vector<vector<char>> test6 = {
            { '0' }
    };

    vector<vector<char>> test7;

    vector<vector<char>> test8 = {
            { '1', '1', '1' },
            { '1', '0', '1' },
            { '1', '1', '1' }
    };

    vector<vector<char>> test9 = {
            { '1', '1', '1' },
            { '1', '0', '1' },
            { '1', '0', '1' }
    };


    assert(1 == numIslands(test1));
    assert(3 == numIslands(test2));
    assert(0 == numIslands(test3));
    assert(1 == numIslands(test4));
    assert(1 == numIslands(test5));
    assert(0 == numIslands(test6));
    assert(0 == numIslands(test7));
    assert(1 == numIslands(test8));
    assert(1 == numIslands(test9));
}

void test_solveSudoku() {
    vector<vector<char>> test1 = {
            { '5', '3', '.', '.', '7', '.', '.', '.', '.' },
            { '6', '.', '.', '1', '9', '5', '.', '.', '.' },
            { '.', '9', '8', '.', '.', '.', '.', '6', '.' },
            { '8', '.', '.', '.', '6', '.', '.', '.', '3' },
            { '4', '.', '.', '8', '.', '3', '.', '.', '1' },
            { '7', '.', '.', '.', '2', '.', '.', '.', '6' },
            { '.', '6', '.', '.', '.', '.', '2', '8', '.' },
            { '.', '.', '.', '4', '1', '9', '.', '.', '5' },
            { '.', '.', '.', '.', '8', '.', '.', '7', '9' },
    };
    solveSudoku(test1);

    assert(true == isSudokuSolution(test1));
}

void test_isValidSudoku() {
    vector<vector<char>> test1 = {
            { '5', '3', '.', '.', '7', '.', '.', '.', '.' },
            { '6', '.', '.', '1', '9', '5', '.', '.', '.' },
            { '.', '9', '8', '.', '.', '.', '.', '6', '.' },
            { '8', '.', '.', '.', '6', '.', '.', '.', '3' },
            { '4', '.', '.', '8', '.', '3', '.', '.', '1' },
            { '7', '.', '.', '.', '2', '.', '.', '.', '6' },
            { '.', '6', '.', '.', '.', '.', '2', '8', '.' },
            { '.', '.', '.', '4', '1', '9', '.', '.', '5' },
            { '.', '.', '.', '.', '8', '.', '.', '7', '9' },
    };
    vector<vector<char>> test2 = {
            { '5', '3', '.', '.', '7', '.', '.', '.', '.' },
            { '5', '.', '.', '1', '9', '5', '.', '.', '.' },
            { '.', '9', '8', '.', '.', '.', '.', '6', '.' },
            { '8', '.', '.', '.', '6', '.', '.', '.', '3' },
            { '4', '.', '.', '8', '.', '3', '.', '.', '1' },
            { '7', '.', '.', '.', '2', '.', '.', '.', '6' },
            { '.', '6', '.', '.', '.', '.', '2', '8', '.' },
            { '.', '.', '.', '4', '1', '9', '.', '.', '5' },
            { '.', '.', '.', '.', '8', '.', '.', '7', '9' },
    };
    vector<vector<char>> test3 = {
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
    };
    vector<vector<char>> test4 = {
            { '1', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '1', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
    };
    vector<vector<char>> test5 = {
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '3', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '3', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.', '.', '.', '.' },
    };

    assert(true == isValidSudoku(test1));
    assert(false == isValidSudoku(test2));
    assert(true == isValidSudoku(test3));
    assert(false == isValidSudoku(test4));
    assert(false == isValidSudoku(test5));
}

void test_zigzagLevelOrder() {
    TreeNode *test1 = new TreeNode(4);
    test1->left = new TreeNode(3);
    test1->right = new TreeNode(5);
    test1->left->left = new TreeNode(2);
    test1->right->right = new TreeNode(6);
    
    vector<vector<int>> result1 = {
        { 4 },
        { 5, 3 },
        { 2, 6 }
    };
    
    assert(result1 == zigzagLevelOrder(test1));
}

void test_maxPathSum() {
    TreeNode *test1 = new TreeNode(10);
    test1->left = new TreeNode(3);
    test1->right = new TreeNode(20);
    test1->left->left = new TreeNode(1);
    test1->left->right = new TreeNode(4);
    test1->right->left = new TreeNode(16);
    test1->right->right = new TreeNode(21);
    
    TreeNode *test2 = new TreeNode(10);
    test2->left = new TreeNode(3);
    test2->right = new TreeNode(20);
    test2->left->left = new TreeNode(1);
    test2->left->right = new TreeNode(4);
    test2->right->left = new TreeNode(18);
    test2->right->right = new TreeNode(21);
    
    TreeNode *test3 = nullptr;
    
    TreeNode *test4 = new TreeNode(-3);
    
    TreeNode *test5 = new TreeNode(2);
    test5->left = new TreeNode(-1);
    
    assert(58 == maxPathSum(test1));
    assert(59 == maxPathSum(test2));
    assert(0 == maxPathSum(test3));
    assert(-3 == maxPathSum(test4));
    assert(2 == maxPathSum(test5));
}

void test_isBalanced() {
    TreeNode *test1 = new TreeNode(4);
    test1->left = new TreeNode(3);
    test1->right = new TreeNode(5);
    TreeNode *test2 = new TreeNode(4);
    test2->left = new TreeNode(3);
    test2->left->left = new TreeNode(2);
    
    assert(true == isBalanced(test1));
    assert(false == isBalanced(test2));
}

void test_reverseBits() {
    assert(964176192 == reverseBits(43261596));
    assert(43261596 == reverseBits(964176192));
    assert(0 == reverseBits(0));
    assert(UINT32_MAX == reverseBits(UINT32_MAX));
}

void test_rotate() {
    int test1[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    int result1[] = { 6, 7, 8, 1, 2, 3, 4, 5 };
    int test2[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    int result2[] = { 7, 8, 1, 2, 3, 4, 5, 6 };
    int test3[] = { 1, 2, 3, 4, 5, 6 };
    int result3[] = { 3, 4, 5, 6, 1, 2 };
    rotate(test1, sizeof(test1) / sizeof(int), 11);
    rotate(test2, sizeof(test2) / sizeof(int), 2);
    rotate(test3, sizeof(test3) / sizeof(int), 4);
    
    for (int i = 0; i < sizeof(test1) / sizeof(int); ++i) {
        assert(test1[i] == result1[i]);
        assert(test2[i] == result2[i]);
        assert(test3[i] == result3[i]);
    }
}

bool isDiff(int i) {
    for (int k = 0; k < 32; ++k) {
        if (i == (1 << k))
            return true;
    }
    return false;
}

void test_isDiff() {
    for (int i = 1; i < 32; ++i) {
        assert(true == isDiff(1 << i));
        assert(false == isDiff((1 << i) + 1));
    }
}

void test_grayCode() {
    vector<int> result1 = { 0, 1, 3, 2 };
    vector<int> result2 = { 0, 2, 3, 1 };
    vector<int> result3 = grayCode(10);
    
    for (size_t i = 0; i < result1.size() - 1; ++i)
        assert(isDiff(result1[i] ^ result1[i + 1]));
    
    for (size_t i = 0; i < result2.size() - 1; ++i)
        assert(isDiff(result2[i] ^ result2[i + 1]));
    
    for (size_t i = 0; i < result3.size() - 1; ++i)
        assert(isDiff(result3[i] ^ result3[i + 1]));
    
}

void test_sortColor() {
    int test1[] = {1, 0, 0, 1, 2};
    int result1[] = {0, 0, 1, 1, 2};
    sortColors(test1, sizeof(test1)/sizeof(int));
    for (int i = 0; i < sizeof(test1)/sizeof(int); ++i)
        assert(test1[i] == result1[i]);
}

void test_simplifyPath() {
    assert(string("/") == simplifyPath("///"));
    assert(string("/") == simplifyPath("/../../.."));
    assert(string("/") == simplifyPath("/../../../"));
    assert(string("/") == simplifyPath("/./././."));
    assert(string("/") == simplifyPath("/././././"));
    assert(string("/") == simplifyPath("/./.././."));
    assert(string("/") == simplifyPath("/./../././"));
    assert(string("/a") == simplifyPath("/a/b/.."));
    assert(string("/a") == simplifyPath("/a/b/../"));
    assert(string("/a/b") == simplifyPath("/a/b/."));
    assert(string("/a/b") == simplifyPath("/a/b/./"));
    assert(string("/.abc") == simplifyPath("/.abc"));
    assert(string("/.abc") == simplifyPath("/.abc/"));
    assert(string("/...") == simplifyPath("/..."));
    assert(string("/...") == simplifyPath("/.../"));
}

void test_setZeroes() {
    vector<vector<int>> test1 = {
        {1, 2, 3},
        {4, 0, 6},
        {7, 8, 9}
    };
    vector<vector<int>> result1 = {
        {1, 0, 3},
        {0, 0, 0},
        {7, 0, 9}
    };
    vector<vector<int>> test2 = {
        {1, 0, 3},
        {4, 0, 6},
        {7, 8, 9}
    };
    vector<vector<int>> result2 = {
        {0, 0, 0},
        {0, 0, 0},
        {7, 0, 9}
    };
    setZeroes(test1);
    setZeroes(test2);
    
    assert(test1 == result1);
    assert(test2 == result2);
}

void test_searchMatrix() {
    vector<vector<int>> a = {
        {1, 3, 5, 7},
        {10, 11, 16,20},
        {23, 30, 34, 50}
    };
    assert(true == searchMatrix(a, 3));
}

void test_nextPermutation() {
    vector<int> a = {3, 2, 1};
    vector<int> a1 = {1, 2, 3};
    nextPermutation(a);

    assert(a == a1);
    
    vector<int> b = {2, 3, 1};
    vector<int> b1 = {3, 1, 2};
    nextPermutation(b);
    assert(b == b1);
    
    vector<int> c = {4, 2, 0, 2, 3, 2, 0};
    vector<int> c1 = {4, 2, 0, 3, 0, 2, 2};
    nextPermutation(c);

    assert(c == c1);
}

void test_firstMissingPositive() {
    int test1[] = { 1, 2, 0 };
    int test2[] = { 3, 4, -1, 1 };
    int test3[] = { 1, 1 };
    
    assert(3 == firstMissingPositive(test1, 3));
    assert(2 == firstMissingPositive(test2, 4));
    assert(2 == firstMissingPositive(test3, 2));
}

void test_calculateMinimumHP() {
    vector<vector<int>> test1 = {
        { -2, -3, 3 },
        { -5, -10, 1 },
        { 10, 30, -5 }
    };
    
    assert(7 == calculateMinimumHP(test1));
}

void test_levelOrderBottom() {
    TreeNode *test1 = new TreeNode(5);
    test1->left = new TreeNode(3);
    test1->right = new TreeNode(6);
    vector<vector<int>> result1 = { { 3, 6 }, { 5 } };
    
    assert(result1 == levelOrderBottom(test1));
}

void test_compareVersion() {
    assert(1 == compareVersion("1.2", "1.1"));
    assert(-1 == compareVersion("1.1", "1.2"));
    assert(0 == compareVersion("1.1", "1.1"));
    assert(-1 == compareVersion("1.1", "1.1.1"));
    assert(1 == compareVersion("1.1.1", "1.1"));
    assert(1 == compareVersion("11", "2.3"));
    assert(-1 == compareVersion("2.3", "11"));
    assert(0 == compareVersion("01", "1"));
    assert(0 == compareVersion("1", "01"));
    assert(1 == compareVersion("1", "0"));
    assert(-1 == compareVersion("0", "1"));
    assert(0 == compareVersion("1.0", "1"));
    assert(0 == compareVersion("1", "1.0"));
    assert(1 == compareVersion("1.0.1", "1"));
    assert(-1 == compareVersion("1", "1.0.1"));
    assert(1 == compareVersion("1.0", "0.1"));
    assert(-1 == compareVersion("0.1", "1.0"));
}

void test_split() {
    vector<string> test1 = { "a", "b", "c" };
    vector<string> test2 = { "2", "3" };
    
    assert(test1 == split("a,b,c", ','));
    assert(test2 == split("2.3", '.'));
    assert(test2 == split("2..3", '.'));
}

void test_stringToInteger() {
    assert(1 == stringToInteger("1"));
    assert(222 == stringToInteger("222"));
    assert(1 == stringToInteger("01"));
    assert(0 == stringToInteger("00"));
}

void test_levelOrder() {
    TreeNode *test1 = new TreeNode(5);
    test1->left = new TreeNode(3);
    test1->right = new TreeNode(6);
    vector<vector<int>> result1 = { { 5 }, { 3, 6 } };
    
    assert(result1 == levelOrder(test1));
}

void test_peakElement() {
    vector<int> test1 = { 3, 1, 2 };
    vector<int> test2 = { 2, 3, 4 };
    vector<int> test3 = { 1, 2, 1 };
    vector<int> test4 = { 1, 2, 3, 4, 5, 7, 6 };
    vector<int> test5 = { 1, 7, 6, 5 };
    
    assert(0 == findPeakElement(test1));
    assert(2 == findPeakElement(test2));
    assert(1 == findPeakElement(test3));
    assert(5 == findPeakElement(test4));
    assert(1 == findPeakElement(test5));
}

void test_majorityElement() {
    vector<int> test1 = { 1, 1, 2 };
    vector<int> test2 = { 1 };
    vector<int> test3 = { 6, 5, 5 };
    
    assert(1 == majorityElement(test1));
    assert(1 == majorityElement(test2));
    assert(5 == majorityElement(test3));
}

void test_BSTIterator() {
    TreeNode *root = new TreeNode(5);
    root->left = new TreeNode(3);
    root->right = new TreeNode(6);
    BSTIterator i = BSTIterator(root);
    vector<int> test1;
    vector<int> result1 = { 3, 5, 6 };
    
    while (i.hasNext())
        test1.push_back(i.next());
    
    assert(test1 == result1);
}

void test_largestNumber() {
    vector<int> test1 = { 0, 0, 0, 0, 0, 0, 0 };
    vector<int> test2 = { 9, 8, 98, 9, 9, 9, 9 };
    vector<int> test3 = { 8, 89 };
    vector<int> test4 = { 10 };
    vector<int> test5 = { 824, 938, 1399, 5607, 6973, 5703, 9609, 4398, 8247 };
    vector<int> test6 = { 121, 12 };
    
    assert(string("0") == largestNumber(test1));
    assert(string("99999988") == largestNumber(test2));
    assert(string("898") == largestNumber(test3));
    assert(string("10") == largestNumber(test4));
    assert(string("9609938824824769735703560743981399") == largestNumber(test5));
}

void test_largestNumber_intToString() {
    assert(string("0") == largestNumber_intToString(0));
    assert(string("10") == largestNumber_intToString(10));
    assert(string("222123") == largestNumber_intToString(222123));
}

void test_largestNumber_compare() {
    assert(false == largerstNumber_compare("2", "222"));
    assert(false == largerstNumber_compare("22", "2"));
    assert(true == largerstNumber_compare("2281", "2"));
    assert(true == largerstNumber_compare("2281", "22"));
    assert(false == largerstNumber_compare("2281", "228"));
    assert(false == largerstNumber_compare("8247", "824"));
    assert(true == largerstNumber_compare("824", "8247"));
}

void test_findRepeatedDnaSequences() {
    auto test1 = findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT");
    sort(test1.begin(), test1.end());
    vector<string> result1 = { "AAAAACCCCC", "CCCCCAAAAA" };
    
    auto test2 = findRepeatedDnaSequences("AAAAAAAAAAA");
    vector<string> result2 = { "AAAAAAAAAA" };
    
    auto test3 = findRepeatedDnaSequences("AAAA");
    vector<string> result3;
    
    // test for duplicate
    auto test4 = findRepeatedDnaSequences("AAAAAAAAAAAA");
    vector<string> result4 = { "AAAAAAAAAA" };
    
    assert(test1 == result1);
    assert(test2 == result2);
    assert(test3 == result3);
    assert(test4 == result4);
}

void test_trailingZeroes() {
    assert(0 == trailingZeroes(4));
    assert(1 == trailingZeroes(9));
    assert(2 == trailingZeroes(10));
    assert(6 == trailingZeroes(25));
}

void test_convertToTitle() {
    assert(string("") == convertToTitle(0));
    assert(string("A") == convertToTitle(1));
    assert(string("AA") == convertToTitle(27));
    assert(string("AB") == convertToTitle(28));
    assert(string("ABC") == convertToTitle(titleToNumber("ABC")));
    assert(string("DEFDS") == convertToTitle(titleToNumber("DEFDS")));
    assert(string("Z") == convertToTitle(titleToNumber("Z")));
    assert(string("AZ") == convertToTitle(titleToNumber("AZ")));
    
}

void test_longestValidParentheses() {
    assert(0 == longestValidParentheses(""));
    assert(4 == longestValidParentheses("()()"));
    assert(0 == longestValidParentheses(")("));
    assert(2 == longestValidParentheses("()("));
    assert(2 == longestValidParentheses("(()"));
    assert(2 == longestValidParentheses("()(()"));
    assert(4 == longestValidParentheses("()(()()"));
    assert(2 == longestValidParentheses("(()(((()"));
    assert(0 == longestValidParentheses("("));
    assert(0 == longestValidParentheses(")"));
    assert(4 == longestValidParentheses("(()()(()()"));
}

void test_hammingWeight() {
    assert(0 == hammingWeight(0));
    assert(32 == hammingWeight(UINT32_MAX));
}

void test_titleToNumber() {
    assert(1 == titleToNumber("A"));
    assert(27 == titleToNumber("AA"));
}

void test_wordSearch() {
    vector<vector<char>> test1 = {
        {'A', 'B', 'C', 'E'},
        {'S', 'F', 'C', 'S'},
        {'A', 'D', 'E', 'E'}
    };
    
    assert(exist(test1, "ABCCED") == true);
    assert(exist(test1, "SEE") == true);
    assert(exist(test1, "ABCB") == false);
    
    vector<vector<char>> test2 = {
        {'A', 'B'}
    };
    assert(exist(test2, "AB") == true);
}

void test_combine() {
    auto test1 = combine(3, 2);
    auto result1 = vector<vector<int>> {
        {1, 2},
        {1, 3},
        {2, 3}
    };
    
    sort(test1.begin(), test1.end());
    
    assert(test1 == result1);
}

void test_cloneGraph() {
    UndirectedGraphNode *node1 = new UndirectedGraphNode(1);
    UndirectedGraphNode *node2 = new UndirectedGraphNode(2);
    UndirectedGraphNode *node3 = new UndirectedGraphNode(3);
    node1->neighbors.push_back(node2);
    node2->neighbors.push_back(node1);
    node1->neighbors.push_back(node3);
    node3->neighbors.push_back(node1);
    node3->neighbors.push_back(node2);
    node2->neighbors.push_back(node3);
    
    // auto result = cloneGraph(node1);
    
}

