//
//  solution.h
//  LeetCode
//
//  Created by RogerChen on 3/12/15.
//  Copyright (c) 2015 RogerChen. All rights reserved.
//

#ifndef __LeetCode__solution__
#define __LeetCode__solution__

#include <float.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <limits.h>

#include <vector>
#include <string>
#include <stack>
#include <list>
#include <queue>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include <algorithm>
#include <limits>
#include <utility>
#include <sstream>
#include <iostream>


using std::set;
using std::pair;
using std::hash;
using std::queue;
using std::stringstream;
using std::getline;
using std::max;
using std::min;
using std::sort;
using std::cout;
using std::binary_search;
using std::unordered_map;
using std::unordered_set;
using std::deque;
using std::max_element;
using std::min_element;
using std::vector;
using std::sort;
using std::swap;
using std::string;
using std::stack;
using std::list;
using std::pair;
using std::reverse;
using std::unique;

struct Point {
    int x;
    int y;
    Point() : x(0), y(0) {}
    Point(int a, int b) : x(a), y(b) {}
};

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

struct UndirectedGraphNode {
    int label;
    vector<UndirectedGraphNode *> neighbors;
    UndirectedGraphNode(int x) : label(x) {};
};

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

struct RandomListNode {
    int label;
    RandomListNode *next, *random;
    RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
};

class BSTIterator {
private:
    stack<TreeNode *> parent_nodes_;
    TreeNode *current_ = nullptr;
public:
    BSTIterator(TreeNode *root) {
        if (root)
            construct(root);
    }
    
    void construct(TreeNode *node) {
        while (node->left) {
            parent_nodes_.push(node);
            node = node->left;
        }
        current_ = node;
    }
    
    // return whether we have next smallest number
    bool hasNext() {
        if (current_)
            return true;
        else
            return false;
    }
    
    // return next smallest number
    int next() {
        int ret = current_->val;
        if (current_ -> right)
            construct(current_->right);
        else {
            if (!parent_nodes_.empty()) {
                current_ = parent_nodes_.top();
                parent_nodes_.pop();
            }
            else
                current_ = nullptr;
        }
        
        return ret;
    }
};

class MinStack {
private:
    stack<int> main_;
    stack<int> min_;
public:
    void push(int x) {
        main_.push(x);
        if (min_.empty() || (!min_.empty() && x <= min_.top())) {
            min_.push(x);
        }
    }
    
    void pop() {
        if (!main_.empty() && main_.top() == min_.top()) {
            min_.pop();
        }
        main_.pop();
    }
    
    int top() {
        return main_.top();
    }
    
    int getMin() {
        return min_.top();
    }
};

class KMP {
private:
    string pattern_;
    vector<vector<size_t>> dfa_;
public:
    KMP(const string &pattern, size_t alphabet_size) : pattern_(pattern), dfa_(alphabet_size) {
        for (auto &&each : dfa_) {
            each.reserve(pattern.size());
            each[0] = 0;
        }
        dfa_[pattern[0]][0] = 1;

        for (size_t restart_state = 0, j = 1; j < pattern.size(); ++j) {
            for (auto i = 0; i < alphabet_size; ++i) {
                dfa_[i][j] = dfa_[i][restart_state];
            }
            dfa_[pattern[j]][j] = j + 1;
            restart_state = dfa_[pattern[j]][restart_state];
        }
    }
    
    KMP(const string &pattern) : KMP(pattern, 256) { }
    
    size_t search(const string &text) {
        size_t i, j;
        for (i = 0, j = 0; i < text.size() && j < pattern_.size(); ++i) {
            j = dfa_[text[i]][j];
        }
        if (j == pattern_.size())
            return i - j;
        else
            return -1;
    }
};

class DiGraph {
private:
    vector<vector<size_t>> adjList_;
public:
    DiGraph(size_t size) : adjList_(size) { }
    void addEdge(size_t vertex1, size_t vertex2) {
        adjList_[vertex1].push_back(vertex2);
    }
    vector<size_t> adjacent(size_t vertex) {
        return adjList_[vertex];
    }
    size_t vertices() {
        return adjList_.size();
    }
};

class DiGraphDFS {
private:
    vector<bool> marked_;
    void dfs(DiGraph g, size_t node) {
        for (auto &&each : g.adjacent(node)) {
            if (!marked_[each]) {
                marked_[each] = true;
                dfs(g, each);
            }
        }
    }
public:
    DiGraphDFS(DiGraph g, size_t vertex) : marked_(g.vertices(), false) {
        marked_[vertex] = true;
        dfs(g, vertex);
    }
    DiGraphDFS(DiGraph g, vector<size_t> vertices) : marked_(g.vertices(), false) {
        for (auto &&vertex : vertices) {
            marked_[vertex] = true;
            dfs(g, vertex);
        }
    }
    
    bool marked(size_t vertex) {
        return marked_[vertex];
    }
};

class NFA {
private:
    DiGraph epsilon_transition;
    string regular_expression;
    size_t accept_state;
public:
    NFA(string regex) : epsilon_transition(regex.size() + 1), regular_expression(regex), accept_state(regex.size()) {
        stack<size_t> operators; // record the position where each operator appears

        for (size_t i = 0; i < regular_expression.size(); ++i) {
            size_t left_parenthesis = i;
            if (regular_expression[i] == '(' || regular_expression[i] == '|') {
                operators.push(left_parenthesis);
            }
            else if (regular_expression[i] == ')') {
                size_t or_operator = operators.top();
                operators.pop();
                if (regular_expression[or_operator] == '|') {
                    left_parenthesis = operators.top();
                    operators.pop();
                    epsilon_transition.addEdge(left_parenthesis, or_operator + 1);
                    epsilon_transition.addEdge(or_operator, i);
                }
                else {
                    left_parenthesis = or_operator;
                }
            }
            if (i < regular_expression.size() - 1 && regular_expression[i + 1] == '*') {
                epsilon_transition.addEdge(left_parenthesis, i + 1);
                epsilon_transition.addEdge(i + 1, left_parenthesis);
            }
            if (regular_expression[i] == '(' || regular_expression[i] == ')' || regular_expression[i] == '*') {
                epsilon_transition.addEdge(i, i + 1);
            }
        }
    }
    bool recognize(const string &text) {
        vector<size_t> available_state;
        DiGraphDFS dfs(epsilon_transition, 0);
        for (size_t v = 0; v < epsilon_transition.vertices(); ++v) {
            if (dfs.marked(v)) {
                available_state.push_back(v);
            }
        }

        vector<size_t> matched;
        for (size_t i = 0; i < text.size(); ++i) {
            for (auto &&state : available_state) {
                if (state < accept_state && (regular_expression[state] == text[i] || regular_expression[state] == '.')) {
                    matched.push_back(state + 1);
                }
            }
            if (matched.empty())
                return false;
            dfs = DiGraphDFS(epsilon_transition, std::move(matched));
            available_state.clear();
            for (size_t v = 0; v < epsilon_transition.vertices(); ++v) {
                if (dfs.marked(v)) {
                    available_state.push_back(v);
                }
            }
            matched.clear();
        }

        for (auto &&state : available_state) {
            if (state == accept_state) {
                return true;
            }
        }
        return false;
    }
};

class LRUCache_TL{
private:
    list<pair<int, int>> cache_;
    size_t size_;
    size_t capacity_;
public:
    LRUCache_TL(int capacity) : capacity_(capacity), size_(0) { }
    
    int get(int key) {
        for (auto iter = cache_.begin(); iter != cache_.end(); ++iter) {
            if (iter->first == key) {
                auto temp = *iter;
                cache_.erase(iter);
                cache_.push_front(std::move(temp));
                return cache_.front().second;
            }
        }
        return -1;
    }
    
    void set(int key, int value) {
        if (size_ < capacity_) {
            cache_.push_front({key, value});
            ++size_;
        }
        else {
            cache_.pop_back();
            cache_.push_front({key, value});
        }
    }
};

class LRUCache{
private:
    list<pair<int, int>> cache_;
    size_t size_;
    size_t capacity_;
public:
    LRUCache(int capacity) : capacity_(capacity), size_(0) { }
    
    int get(int key) {
        for (auto iter = cache_.begin(); iter != cache_.end(); ++iter) {
            if (iter->first == key) {
                auto temp = *iter;
                cache_.erase(iter);
                cache_.push_front(std::move(temp));
                return cache_.front().second;
            }
        }
        return -1;
    }
    
    void set(int key, int value) {
        if (size_ < capacity_) {
            cache_.push_front({key, value});
            ++size_;
        }
        else {
            cache_.pop_back();
            cache_.push_front({key, value});
        }
    }
};

int lengthOfLIS(vector<int>& nums);
int minCut(string s);
bool isPalindrome(string s);
int rob_circle(vector<int>& nums);
int numDecodings(string s);
string fractionToDecimal(int numerator, int denominator);
int ladderLength(string beginWord, string endWord, unordered_set<string>& wordDict);
void surroundedRegion(vector<vector<char>> &board);
string getPermutation(int n, int k);
bool next_permutation(vector<int> &num);
vector<vector<int>> permuteUnique(vector<int>& nums);
int maximumGap(vector<int>& nums);
double myPow(double x, int n);
int maxPoints(vector<Point>& points);
int evalRPN(vector<string>& tokens);
ListNode* sortList(ListNode* head);
bool isSymmetric(TreeNode *root);
bool isSameTree(TreeNode *p, TreeNode *q);
RandomListNode *copyRandomList(RandomListNode *head);
TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder);
vector<vector<int> > combinationSum2(vector<int> &candidates, int target);
vector<vector<int> > combinationSum(vector<int> &candidates, int target);
string addBinary(string a, string b);
string countAndSay(int n);
int rob(vector<int> &num);
bool isSudokuSolution(vector<vector<char> > &board);
void solveSudoku(vector<vector<char> > &board);
bool isValidSudoku(vector<vector<char> > &board);
int numIslands(vector<vector<char>> &grid);
int calculateMinimumHP(vector<vector<int> > &dungeon);
int maxProfit(vector<int> &prices);
int maxPathSum(TreeNode *root);
vector<vector<int> > zigzagLevelOrder(TreeNode *root);
bool isBalanced(TreeNode *root);
uint32_t reverseBits(uint32_t n);
void rotate(int nums[], int n, int k);
vector<int> grayCode(int n);
vector<vector<int> > combine(int n, int k);
void sortColors(int A[], int n);
string simplifyPath(string path);
void setZeroes(vector<vector<int> > &matrix);
bool searchMatrix(vector<vector<int>> &matrix, int target);
int strStr(const char *haystack, const char *needle);
void nextPermutation(vector<int> &num);
int firstMissingPositive(int A[], int n);
int calculateMinimumHP(vector<vector<int> > &dungeon);
int stringToInteger(const string &s);
vector<string> split(const string &s, char delim);
vector<vector<int>> levelOrderBottom(TreeNode *root);
vector<vector<int> > levelOrder(TreeNode *root);
int compareVersion(string version1, string version2);
int findPeakElement(const vector<int> &num);
int majorityElement(vector<int> &num);
bool largerstNumber_compare(const string &lhs, const string &rhs);
string largestNumber_intToString(int n);
string largestNumber(vector<int> &num);
vector<string> findRepeatedDnaSequences(string s);
int trailingZeroes(int n);
string convertToTitle(int n);
int titleToNumber(string s);
int hammingWeight(uint32_t n);
int longestValidParentheses(string s);
bool exist(vector<vector<char>> &board, string word);
UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node);
vector<vector<int>> combine(int n, int k);
vector<int> largestDivisibleSubset(vector<int>& nums);
int largestDivisibleSubset_nextK(const vector<int>& nums, const vector<int> &opt, int k);

#endif /* defined(__LeetCode__solution__) */
