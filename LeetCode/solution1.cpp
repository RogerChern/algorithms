//
// Created by RogerChen on 7/7/16.
// Too many implementations jammed in a single file yields very slow parsing
//

#include "solution.h"

#pragma clang diagnostic push
#pragma ide dia gnostic ignored "TemplateArgumentsIssues"

// find one of the longest increasing sequence
vector<int> LIS(const vector<int>& nums) {
    if (nums.size() <= 1) return nums;

    vector<int> opt(nums.size());
    // rec[i] gives the index of the previous element in the IS it belongs.
    vector<int> rec(nums.size(), -1);
    opt[0] = 1;

    for (int k = 1; k < opt.size(); ++k) {
        vector<int> candidate;
        // find all i, i < k && nums[i] < nums[k]
        for (int i = 0; i < k; ++i) {
            if (nums[i] < nums[k]) candidate.push_back(i);
        }
        if (candidate.size() == 0) {
            opt[k] = 1;
        } else {
            // find max(opt[i]) and its index s.t. i < k && nums[i] < nums[k]
            int max_idx = -1;
            int max_opt = -1;
            for (int i = 0; i < candidate.size(); ++i) {
                if (max_opt < opt[candidate[i]]) {
                    max_opt = opt[candidate[i]];
                    max_idx = candidate[i];
                }
            }
            rec[k] = max_idx;
            opt[k] = 1 + max_opt;
        }
    }
    // find the longest IS
    int max_opt = -1;
    int max_idx = -1;
    for (int i = 0; i < opt.size(); ++i) {
        if (max_opt < opt[i]) {
            max_opt = opt[i];
            max_idx = i;
        }
    }
    // backtracking in rec to retrieve all the elements of LIS
    vector<int> ret;
    while (max_idx != -1) {
        ret.push_back(nums[max_idx]);
        max_idx = rec[max_idx];
    }
    return ret;
}

int lengthOfLIS(vector<int>& nums) {
    if (nums.size() <= 1) return nums.size();

    vector<int> opt(nums.size());
    opt[0] = 1;
    for (int k = 1; k < opt.size(); ++k) {
        vector<int> candidate;
        for (int i = 0; i < k; ++i) {
            if (nums[i] < nums[k]) candidate.push_back(opt[i]);
        }
        if (candidate.size() == 0) {
            opt[k] = 1;
        } else {
            opt[k] = 1 + *std::max_element(candidate.begin(), candidate.end());
        }
    }
    return *std::max_element(opt.begin(), opt.end());
}

int minCut(string s) {
    if (s.size() <= 1) return 0;

    vector<int> opt(s.size()+1, 0);
    vector<vector<bool>> isPalindrome;
    for (int i = 0; i < s.size(); ++i) {
        isPalindrome.emplace_back(s.size(), true);
    }
    for (int i = 0; i < s.size(); ++i) {
        for (int j = i-1; j >= 0 ; --j) {
            if (!(s[j] == s[i] && (i-j < 2 || isPalindrome[i-1][j+1]))) isPalindrome[i][j] = false;
        }
    }

    opt[0] = -1;
    for (size_t k = 1; k < opt.size(); ++k) {
        vector<int> candidate;
        for (size_t i = k; i > 0; --i) {
            if (isPalindrome[k-1][i-1]) {
                candidate.push_back(opt[i-1]);
            }
        }
        opt[k] = 1 + *std::min_element(candidate.begin(), candidate.end());
    }
    return opt.back();
}

bool isPalindrome(string s) {
    string r = s;
    std::reverse(r.begin(), r.end());
    return s == r;
}

int rob_circle(vector<int>& nums) {
    if (nums.empty()) return 0;
    if (nums.size() == 1) return nums[0];
    if (nums.size() == 2) return std::max(nums[0], nums[1]);

    vector<int> helper1(nums.begin()+2, nums.end()-1);
    vector<int> helper2(nums.begin()+1, nums.end());
    return max(nums[0]+rob(helper1), rob(helper2));
}

int rob(vector<int>& nums) {
    if (nums.empty()) return 0;
    if (nums.size() == 1) return nums[0];

    vector<int> opt(nums.size(), 0);
    opt[0] = nums[0];
    opt[1] = std::max(nums[0], nums[1]);
    for (int i = 2; i < opt.size(); ++i) {
        opt[i] = std::max(opt[i - 2] + nums[i], opt[i - 1]);
    }
    return opt.back();
}

vector<int> largestDivisibleSubset(vector<int>& nums) {
    if (nums.size() <= 1) return nums;

    sort(nums.begin(), nums.end());
    nums.insert(nums.begin(), 0);
    vector<int> opt(nums.size(), 0);
    vector<int> rec(nums.size(), -1);
    opt[1] = 1;
    rec[1] = 0;

    for (int k = 2; k < opt.size(); ++k) {
        int nextK = largestDivisibleSubset_nextK(nums, opt, k);
        opt[k] = 1 + opt[nextK];
        rec[k] = nextK;
    }

    int idx = 0;
    int max = 0;
    for (int i = 1; i < opt.size(); ++i) {
        if (max < opt[i]) {
            max = opt[i];
            idx = i;
        }
    }

    vector<int> ret;
    while (idx != 0) {
        ret.push_back(nums[idx]);
        idx = rec[idx];
    }

    return ret;
}

int largestDivisibleSubset_nextK(const vector<int>& nums, const vector<int> &opt, int k) {
    int max = 0;
    int idx = 0; // 0 for not found
    for (int i = k-1; i > 0; --i) {
        if (nums[k] % nums[i] == 0) {
            if (max < opt[i]) {
                max = opt[i];
                idx = i;
            }
        }
    }
    return idx;
}

bool is_differ_at_one(const string &lhs, const string &rhs) {
    int diff = 0;
    for (int i = 0; i < lhs.size(); ++i) {
        if (diff > 1) {
            return false;
        }
        if (lhs[i] != rhs[i]) {
            ++diff;
        }
    }
    return diff == 1;
}

int ladderLength(string beginWord, string endWord, unordered_set<string>& wordDict) {
    unordered_map<string, vector<string>> graph;
    unordered_map<string, bool> marked;
    wordDict.insert(beginWord);
    wordDict.insert(endWord);

    for (auto &&i : wordDict) {
        graph[i] = {};
        marked[i] = false;
    }

    for (auto i = wordDict.cbegin(); i != wordDict.cend(); ++i) {
        for (auto j = i; j != wordDict.cend(); ++j) {
            if (is_differ_at_one(*i, *j)) {
                graph[*i].push_back(*j);
                graph[*j].push_back(*i);
            }
        }
    }

    queue<pair<string, int>> transverse_queue;
    transverse_queue.push({beginWord, 1});
    marked[beginWord] = true;

    while (!transverse_queue.empty()) {
        auto node = transverse_queue.front();
        transverse_queue.pop();
        for (auto &&i : graph[node.first]) {
            if (i == endWord) {
                return node.second + 1;
            }
            if (!marked[i]) {
                marked[i] = true;
                transverse_queue.push({i, node.second+1});
            }
        }
    }

    return 0;
}

int numDecodings_solver(string s, vector<int> &state) {
    // read cache
    if (state[s.size()] != -1) return state[s.size()];

    // base case
    if (s.size() == 0) {
        state[s.size()] = 0;
        return 0;
    }
    if (s[0] == '0') {
        // after null check
        state[s.size()] = 0;
        return 0;
    }
    if (s.size() == 1) {
        state[s.size()] = 1;
        return 1;
    }
    if (s.size() == 2) {
        int parsed_first_two = (s[0] - '0') * 10 + s[1] - '0';
        if (parsed_first_two <= 26 && parsed_first_two > 0) {
            int result = numDecodings_solver(s.substr(1), state) + 1;
            state[s.size()] = result;
            return result;
        } else {
            // first digit has been tested
            int result = numDecodings_solver(s.substr(1), state);
            state[s.size()] = result;
            return result;
        }
    }
    // s.size() > 2
    int parsed_first_two = (s[0] - '0') * 10 + s[1] - '0';
    if (parsed_first_two <= 26 && parsed_first_two > 0) {
        int result = numDecodings_solver(s.substr(1), state) + numDecodings_solver(s.substr(2), state);
        state[s.size()] = result;
        return result;
    } else if (s[1] != '0') {
        int result = numDecodings_solver(s.substr(1), state);
        state[s.size()] = result;
        return result;
    } else {
        state[s.size()] = 0;
        return 0;
    }
}

int numDecodings(string s) {
    vector<int> state(s.size()+1, -1);
    numDecodings_solver(s, state);
    return state[s.size()];
}

string fractionToDecimal(int numerator, int denominator) {
    double sign = double(numerator) / denominator;

    int64_t numer = static_cast<int64_t>(numerator);
    int64_t denom = static_cast<int64_t>(denominator);
    numer = numer > 0 ? numer : -numer;
    denom = denom > 0 ? denom : -denom;

    vector<int64_t> quotients;
    vector<int64_t> remainders;

    // deal with the integer part
    quotients.push_back(numer / denom);
    remainders.push_back(numer % denom);

    // deal with the fraction part
    uint64_t pos;
    bool cycle = true;
    bool cycle_found = false;
    while (true) {
        int64_t new_denom = remainders.back() * 10;
        int64_t quotient = new_denom / denom;
        int64_t remainder = new_denom % denom;
        for (pos = 1; pos < quotients.size(); ++pos) {
            if (quotients[pos] == quotient && remainders[pos] == remainder) {
                cycle_found = true;
                break;
            }
        }
        if (cycle_found) break;
        quotients.push_back(quotient);
        remainders.push_back(remainder);
        if (remainder == 0) {
            cycle = false;
            break;
        }
    }
    string ret;
    stringstream tmp;
    if (sign < 0) tmp << "-";
    tmp << quotients[0] << ".";
    if (cycle) {
        for (auto i = 1; i != pos; ++i) {
            tmp << quotients[i];
        }
        tmp << "(";
        for (auto i = pos; i < quotients.size(); ++i) {
            tmp << quotients[i];
        }
        tmp << ")";
    } else {
        for (auto i = 1; i < quotients.size(); ++i) {
            tmp << quotients[i];
        }
    }
    tmp >> ret;
    if (ret[ret.size()-1] == '0' && ret[ret.size()-2] == '.') {
        ret.pop_back();
        ret.pop_back();
    }
    return ret;
}
