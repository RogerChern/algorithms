//
//  solution.cpp
//  LeetCode
//
//  Created by RogerChen on 3/12/15.
//  Copyright (c) 2015 RogerChen. All rights reserved.
//

#include "solution.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "TemplateArgumentsIssues"

void surroundedRegion_dfs(vector<vector<char>> &board, vector<pair<int, int>> &marked, int row, int col) {
    static vector<int> dy = {1, -1, 0, 0};
    static vector<int> dx = {0, 0, 1, -1};

    board[row][col] = 'N';
    marked.push_back({row, col});
    
    for (int i = 0; i < 4; ++i) {
        int nx = col + dx[i];
        int ny = row + dy[i];
        
        if (nx >= 0 && nx < board.front().size() && ny >= 0 && ny < board.front().size() && board[ny][nx] == 'O') {
            surroundedRegion_dfs(board, marked, ny, nx);
        }
    }
}

void surroundedRegion_bfs(vector<vector<char>> &board, vector<pair<int, int>> &marked, int row, int col) {
    queue<pair<int, int>> neighbors;
    vector<int> dy = {1, -1, 0, 0};
    vector<int> dx = {0, 0, 1, -1};
    
    neighbors.push({row, col});
    board[row][col] = 'N';
    
    while (!neighbors.empty()) {
        auto node = neighbors.front();
        neighbors.pop();
        marked.push_back(std::move(node));
        for (int i = 0; i < 4; ++i) {
            int ny = node.first + dy[i];
            int nx = node.second + dx[i];
            
            if (ny >= 0 && ny < board.size() && nx >= 0 && nx < board.front().size() && board[ny][nx] == 'O') {
                neighbors.push({ny, nx});
                board[ny][nx] = 'N';
            }
        }
    }
}

void surroundedRegion(vector<vector<char>> &board) {
    if (board.size() < 3 || board.front().size() < 3) {
        return;
    }
    
    int rows = static_cast<int>(board.size());
    int cols = static_cast<int>(board.front().size());
    
    bool is_surrounded = true;
    vector<pair<int, int>> marked;
    
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (board[i][j] == 'O') {
                surroundedRegion_dfs(board, marked, i, j);
                for (auto &&each : marked) {
                    if (each.first == 0 || each.first == rows - 1 || each.second == 0 || each.second == cols - 1) {
                        is_surrounded = false;
                        break;
                    }
                }
                if (is_surrounded) {
                    for (auto &&i : marked) {
                        board[i.first][i.second] = 'X';
                    }
                }
                is_surrounded = true;
                marked.clear();
            }
        }
    }
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (board[i][j] == 'N')
                board[i][j] = 'O';
        }
    }
}

string getPermutation(int n, int k) {
    // pre-compute n! for further usage
    vector<int> product(n+1);
    product[0] = 1;
    for (int i = 1; i <= n; ++i) {
        product[i] = product[i - 1] * i;
    }

    if (k > product[n]) {
        return "";
    }

    string ret;
    for (char i = '1'; i <= static_cast<char>(n + '0'); ++i) {
        ret += i;
    }

    int base;
    int quotient;
    while (k > 1) {
        for (base = 1; base + 1 < n; ++base) {
            if (k < product[base + 1]) {
                break;
            }
            if (k == product[base + 1]) {
                reverse(ret.end() - (base + 1), ret.end()); // [end - n, end) contains last n elements
                return ret;
            }
        }
        // if we swap end-n with end-n+m, we can generate the m*(n-1)!+1 ~ (m+1)*(n-1)!th permutation
        // k-1 >= 2 here
        quotient = (k-1) / product[base];
        swap(ret[n - (base + 1)], ret[n - (base + 1) + quotient]);
        sort(ret.end() - base, ret.end());
        k = k - quotient * product[base];
    }
    return ret;
}

vector<vector<int>> permuteUnique(vector<int>& nums) {
    if (nums.empty()) {
        return {};
    }
    if (nums.size() == 1) {
        return { nums };
    }
    sort(nums.begin(), nums.end());
    vector<vector<int>> ret;

    do {
        ret.push_back(nums);
    }
    while (next_permutation(nums) == false);

    return ret;
}

int maximumGap(vector<int>& nums) {
    if (nums.size() < 2)
        return 0;
    
    int max = *max_element(nums.begin(), nums.end());
    int min = *min_element(nums.begin(), nums.end());
    
    double interval_length = double(max - min) / nums.size();
    
    vector<int> max_in_interval(nums.size(), INT_MIN);
    vector<int> min_in_interval(nums.size(), INT_MAX);
    
    for (auto &&each : nums) {
        size_t index = (each - min) / interval_length; // auto ceiling
        if (index == nums.size()) {  // in considering of float-point number round up
            index = nums.size() - 1;
        }
        if (max_in_interval[index] < each) {
            max_in_interval[index] = each;
        }
        if (min_in_interval[index] > each) {
            min_in_interval[index] = each;
        }
    }
    
    int gap = 0;
    int max_in_previous_interval = max_in_interval[0];
    for (size_t i = 0; i < nums.size() - 1; ++i) {
        if (min_in_interval[i + 1] == INT_MAX) { // skip the empty interval
            continue;
        }
        else {
            if (gap < min_in_interval[i + 1] - max_in_previous_interval) {
                gap = min_in_interval[i + 1] - max_in_previous_interval;
            }
            max_in_previous_interval = max_in_interval[i + 1];
        }
    }
    return gap;
}

double myPow(double x, int n) {
    if (n == -2)
        return 1 / (x * x);
    if (n == -1)
        return 1 / x;
    if (n == 0)
        return 1.0f;
    if (n == 1)
        return x;
    if (n == 2)
        return x * x;
    
    if (n > 0) {
        if (n % 2 == 1)
            return myPow(myPow(x, n/2), 2) * x;
        else
            return myPow(myPow(x, n/2), 2);
    }
    else {
        if (n % 2 == -1)
            return myPow(myPow(x, n/2), 2) / x;
        else
            return myPow(myPow(x, n/2), 2);
    }
}

int maxPoints(vector<Point>& points) {
    if (points.empty())
        return 0;

    int duplicates;
    int max = 0;
    int local_max;
    unordered_map<double, int> slopes;

    for (auto &&p : points) {
        Point &origin = p;
        duplicates = 0;
        local_max = 0;
        slopes.clear();

        for (auto &&p : points) {
            if (p.x == origin.x && p.y == origin.y) {
                ++duplicates;
                continue;
            }
            if (p.x == origin.x) {
                ++slopes[std::numeric_limits<double>::max()];
            }
            else {
                ++slopes[static_cast<double>(p.y - origin.y) / static_cast<double>(p.x - origin.x)];
            }
        }

        // Do not seperate the calculation of max and duplicates
        for (auto &&iter = slopes.cbegin(); iter != slopes.cend(); ++iter) {
            if (local_max < iter->second) {
                local_max = iter->second;
            }
        }

        if (max < local_max + duplicates) {
            max = local_max + duplicates;
        }
    }

    return max;
}

int evalRPN(vector<string>& tokens) {
    if (tokens.empty())
        return 0;
    
    stack<int> operands;
    for (auto &&x : tokens) {
        if (x != "+" && x != "-" && x != "*" && x != "/") {
            operands.push(std::stoi(x));
        }
        else {
            int second = operands.top();
            operands.pop();
            int first = operands.top();
            operands.pop();
            
            if (x == "+") {
                operands.push(first + second);
            }
            else if (x == "-") {
                operands.push(first - second);
            }
            else if (x == "*") {
                operands.push(first * second);
            }
            else if (x == "/") {
                operands.push(first / second);
            }
        }
    }
    
    if (!operands.empty())
        return operands.top();
    else
        return 0;
}

ListNode * sortList_split(ListNode *head) {
    ListNode *slow = head;
    ListNode *fast = head;
    while (fast->next != nullptr) {
        fast = fast->next;
        if (fast->next != nullptr) {
            slow = slow->next;
            fast = fast->next;
        }
    }
    ListNode *temp = slow->next;
    slow->next = nullptr;
    return temp;
}

ListNode * sortList_merge(ListNode *first, ListNode *second) {
    if (first == nullptr)
        return second;
    if (second == nullptr)
        return first;
    
    ListNode *ret = new ListNode(0);
    ListNode *iter = ret;
    while (first != nullptr && second != nullptr) {
        if (first->val <= second->val) {
            iter->next = first;
            first = first->next;
        }
        else {
            iter->next = second;
            second = second->next;
        }
        iter = iter->next;
    }
    if (first != nullptr)
        iter->next = first;
    if (second != nullptr)
        iter->next = second;
    
    return ret->next;
}

ListNode * sortList(ListNode *head) {
    if (head == nullptr || head->next == nullptr)
        return head;
    ListNode *first = head;
    ListNode *second = sortList_split(head);
    
    first = sortList(first);
    second = sortList(second);
    
    return sortList_merge(first, second);
}

bool isSymmetric_solver(TreeNode *lhs, TreeNode *rhs) {
    if (lhs == nullptr && rhs == nullptr)
        return true;
    if (lhs == nullptr || rhs == nullptr)
        return false;
    if (lhs->val != rhs->val)
        return false;
    return isSymmetric_solver(lhs->right, rhs->left) && isSymmetric_solver(lhs->left, rhs->right);
}

bool isSymmetric(TreeNode *root) {
    if (root == nullptr)
        return true;
    return isSymmetric_solver(root->left, root->right);
}

bool isSameTree(TreeNode *p, TreeNode *q) {
    if (p == nullptr && q == nullptr)
        return true;
    if (p == nullptr)
        return false;
    if (q == nullptr)
        return false;

    if (p->val != q->val)
        return false;

    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
}

RandomListNode *copyRandomList(RandomListNode *head) {
    unordered_map<RandomListNode *, RandomListNode *> map;
    RandomListNode *ret_head = new RandomListNode(0);
    RandomListNode *ret = ret_head;
    RandomListNode *iter = head;
    
    while (iter != nullptr) {
        ret->next = new RandomListNode(iter->label);
        map[iter] = ret->next;
        iter = iter->next;
        ret = ret->next;
    }
    
    ret = ret_head->next;
    while (head != nullptr) {
        ret->random = map[head->random];
        head = head->next;
        ret = ret->next;
    }
    
    return ret_head->next;
}

TreeNode *buildTree_recursive(vector<int> &inorder, int il, int ih, vector<int> &postorder, int pl, int ph) {
    if (ih - il < 0)
        return nullptr;
    if (ih - il == 0)
        return new TreeNode(inorder[il]);
    
    int root = postorder[ph];
    int i;
    for (i = il; i <= ih; ++i) {
        if (inorder[i] == root)
            break;
    }
    
    TreeNode *ret = new TreeNode(root);
    ret->left = buildTree_recursive(inorder, il, i-1, postorder, pl, pl+i-il-1);
    ret->right = buildTree_recursive(inorder, i+1, ih, postorder, ph-ih+i, ph-1);
    
    return ret;
}

TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder) {
    return buildTree_recursive(inorder, 0, int(inorder.size()-1), postorder, 0, int(postorder.size()-1));
}

void combinationSum2_solver(vector<vector<int> > &ret, vector<int> &solution, vector<int> &candidates, int target, int n) {
    if (target == 0) {
        ret.push_back(solution);
    }
    
    for (int i = n; i < candidates.size() && candidates[i] <= target; ++i) {
        // we don't pick the same element twice in a single level
        if (i - n && candidates[i] == candidates[i - 1]) {
            continue;
        }
        solution.push_back(candidates[i]);
        combinationSum2_solver(ret, solution, candidates, target - candidates[i], i + 1);
        solution.pop_back();
    }
}

vector<vector<int> > combinationSum2(vector<int> &candidates, int target) {
    sort(candidates.begin(), candidates.end());
    vector<vector<int> > ret;
    vector<int> solution;
    combinationSum2_solver(ret, solution, candidates, target, 0);
    return ret;
}

void combinationSum_solver(vector<vector<int> > &ret, vector<int> &solution, vector<int> &candidates, int target, int n) {
    if (target == 0) {
        ret.push_back(solution);
    }
    
    for ( ; n < candidates.size() && candidates[n] <= target; ++n) {
        solution.push_back(candidates[n]);
        // same element can be used multiple times
        combinationSum_solver(ret, solution, candidates, target - candidates[n], n);
        solution.pop_back();
    }
}

vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
    sort(candidates.begin(), candidates.end());
    vector<vector<int> > ret;
    vector<int> solution;
    combinationSum_solver(ret, solution, candidates, target, 0);
    return ret;
}

string addBinary(string a, string b) {
    if (a.empty())
        return b;
    if (b.empty())
        return a;
    
    vector<char> s1;
    vector<char> s2;
    string ret;
    
    for (int i = static_cast<int>(a.size() - 1); i >= 0; --i) {
        s1.push_back(a[i] - '0');
    }
    for (int i = static_cast<int>(b.size() - 1); i >= 0; --i) {
        s2.push_back(b[i] - '0');
    }
    
    int n = static_cast<int>(min(a.size(), b.size()));
    
    int i = 0;
    bool carry = false;
    
    // using swap over control statement to enfore DRY
    if (a.size() < b.size()) {
        s1.swap(s2);
    }
    
    while (i < n) {
        s1[i] += s2[i];
        ++i;
    }
    for (auto &&c : s1) {
        if (carry) {
            c += 1;
            carry = false;
        }
        if (c > 1) {
            c %= 2;
            carry = true;
        }
    }
    if (carry) {
        s1.push_back(1);
    }
    for (int i = static_cast<int>(s1.size() - 1); i >=0; --i) {
        ret += s1[i] + '0';
    }
    
    return ret;
}

string countAndSay(int n) {
    if (n <= 0)
        return {};
    
    static vector<string> ret = { "1" };
    
    if (n > ret.size()) {
        string temp;
        string result;
        int count = static_cast<int>(n - ret.size());
        int occurrence = 1;
        int i;
        
        while (count != 0) {
            temp = ret.back();
            for (i = 0; i < temp.size(); ++i) {
                // while loop brings the pointer to the last occurence of consecutive chars
                // for loop changes the char
                while (i < temp.size() - 1 && temp[i] == temp[i+1]) {
                    ++occurrence;
                    ++i;
                }
                
                result += static_cast<char>(occurrence + '0');
                result += temp[i];
                occurrence = 1;
            }
            
            ret.push_back(result);
            result.clear();
            --count;
        }
    }
    
    return ret[n-1];
}

int rob_recursive(vector<int> &num, unordered_map<int, int> &result, int n) {
    if (n == num.size() - 1) {
        if (result.find(n) == result.end()) {
            result[n] = num.back();
        }
        return result[n];
    }
    if (n == num.size() - 2) {
        if (result.find(n) == result.end()) {
            result[n] = max(num[num.size()-1], num[num.size()-2]);
        }
        return result[n];
    }
    if (n == num.size() - 3) {
        if (result.find(n) == result.end()) {
            result[n] = max(num[num.size()-1]+num[num.size()-3], num[num.size()-2]);
        }
        return result[n];
    }
    if (result.find(n) == result.end()) {
        result[n] = num[n] + rob_recursive(num, result, n+2);
    }
    if (result.find(n+1) == result.end()) {
        result[n+1] = num[n+1] + rob_recursive(num, result, n+3);
    }
    return max(result[n], result[n+1]);
}

int rob_old(vector<int> &num) {
    if (num.empty())
        return 0;
    
    unordered_map<int, int> result;
    return rob_recursive(num, result, 0);
}

bool solveSudoku_checkCell(vector<vector<char>> &board, int row, int column) {
    static vector<int> existed(10, 0);
    for (auto &&x : existed)
        x = 0;

    row = row / 3 * 3;
    column = column / 3 * 3;

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            if (board[row+i][column+j] == '.')
                continue;
            if (existed[board[row+i][column+j] - '0'])
                return false;
            else
                existed[board[row+i][column+j] - '0'] = 1;
        }
    }
    return true;
}

bool solveSudoku_checkRow(vector<vector<char>> &board, int row) {
    static vector<int> existed(10, 0);
    for (auto &&x : existed)
        x = 0;

    for (int i = 0; i < 9; ++i) {
        if (board[row][i] == '.')
            continue;
        if (existed[board[row][i] - '0'])
            return false;
        else
            existed[board[row][i] - '0'] = 1;
    }
    return true;
}

bool solveSudoku_checkColumn(vector<vector<char>> &board, int column) {
    static vector<int> existed(10, 0);
    for (auto &&x : existed)
        x = 0;

    for (int i = 0; i < 9; ++i) {
        if (board[i][column] == '.')
            continue;
        if (existed[board[i][column] - '0'])
            return false;
        else
            existed[board[i][column] - '0'] = 1;
    }
    return true;
}

bool solveSudoku_backtrack(vector<vector<char>> &board, int count) {
    if (count == 81)
        return true;

    int row = count / 9;
    int column = count % 9;

    if (board[row][column] == '.') {
        for (char i = '1'; i <= '9'; ++i) {
            board[row][column] = i;
            if (solveSudoku_checkCell(board, row, column) && solveSudoku_checkRow(board, row) && solveSudoku_checkColumn(board, column)) {
                if (solveSudoku_backtrack(board, count + 1))
                    return true;
            }
        }
        board[row][column] = '.';
        return false;
    }
    else {
        if (solveSudoku_backtrack(board, count+1))
            return true;
        else
            return false;
    }
}

void solveSudoku(vector<vector<char> > &board) {
    solveSudoku_backtrack(board, 0);
}

bool isSudokuSolution(vector<vector<char> > &board) {
    vector<int> existed(10, 0);
    // check each 3x3 cell
    for (int i = 0; i < 9; i += 3) {
        for (int j = 0; j < 9; j += 3) {
            // inner loop checking 3x3 area
            for (int x = 0; x < 3; ++x) {
                for (int y = 0; y < 3; ++y) {
                    if (board[i+x][j+y] == '.' || existed[board[i+x][j+y] - '0'])
                        return false;
                    else
                        existed[board[i+x][j+y] - '0'] = 1;
                }
            }
            // clear existed mark
            for (auto &&x : existed)
                x = 0;
        }
    }

    // check each row
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (board[i][j] == '.' || existed[board[i][j] - '0'])
                return false;
            else
                existed[board[i][j] - '0'] = 1;
        }
        for (auto &&x : existed)
            x = 0;
    }

    // check each column
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (board[j][i] == '.' || existed[board[j][i] - '0'])
                return false;
            else
                existed[board[j][i] - '0'] = 1;
        }
        for (auto &&x : existed)
            x = 0;
    }
    return true;
}

bool isValidSudoku(vector<vector<char> > &board) {
    vector<int> existed(10, 0);
    // check each 3x3 cell
    for (int i = 0; i < 9; i += 3) {
        for (int j = 0; j < 9; j += 3) {
            // inner loop checking 3x3 area
            for (int x = 0; x < 3; ++x) {
                for (int y = 0; y < 3; ++y) {
                    if (board[i+x][j+y] == '.')
                        continue;
                    if (existed[board[i+x][j+y] - '0'])
                        return false;
                    else
                        existed[board[i+x][j+y] - '0'] = 1;
                }
            }
            // clear existed mark
            for (auto &&x : existed)
                x = 0;
        }
    }

    // check each row
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (board[i][j] == '.')
                continue;
            if (existed[board[i][j] - '0'])
                return false;
            else
                existed[board[i][j] - '0'] = 1;
        }
        for (auto &&x : existed)
            x = 0;
    }

    // check each column
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (board[j][i] == '.')
                continue;
            if (existed[board[j][i] - '0'])
                return false;
            else
                existed[board[j][i] - '0'] = 1;
        }
        for (auto &&x : existed)
            x = 0;
    }

    return true;
}

void dfs_numIslands(vector<vector<char>> &grid, int i, int j) {
    grid[i][j] = '0';
    if (i > 0 && grid[i-1][j] == '1')
        dfs_numIslands(grid, i-1, j);
    if (j > 0 && grid[i][j-1] == '1')
        dfs_numIslands(grid, i, j-1);
    if (i < grid.size()-1 && grid[i+1][j] == '1')
        dfs_numIslands(grid, i+1, j);
    if (j < grid[0].size()-1 && grid[i][j+1] == '1')
        dfs_numIslands(grid, i, j+1);
}

int numIslands(vector<vector<char>> &grid) {
    int count = 0;
    for (int i = 0; i < grid.size(); ++i) {
        for (int j = 0; j < grid[0].size(); ++j)
            if (grid[i][j] == '1') {
                ++count;
                dfs_numIslands(grid, i, j);
            }
    }
    return count;
}

// @non-complete
int maxProfit(vector<int> &prices) {
    return 0;
}

int maxPathSum_traverse(TreeNode *node, int &max_sum) {
    if (node == nullptr)
        return 0;
    int left = maxPathSum_traverse(node->left, max_sum);
    int right = maxPathSum_traverse(node->right, max_sum);
    
    if (left < 0 && right < 0) {
        if (max_sum < node->val)
            max_sum = node->val;
        return node->val;
    }
    else if (left < 0) {
        if (max_sum < node->val + right)
            max_sum = node->val + right;
        return node->val + right;
    }
    else if (right < 0) {
        if (max_sum < node->val + left)
            max_sum = node->val + left;
        return node->val + left;
    }
    else {
        if (max_sum < node->val + left + right)
            max_sum = node->val + left + right;
        return left > right ? node->val + left : node->val + right;
    }
}

int maxPathSum(TreeNode *root) {
    if (root == nullptr)
        return 0;
    
    int sum = INT_MIN;
    maxPathSum_traverse(root, sum);
    
    return sum;
}

// @non-complete
void zigzagLevelOrder_recursive(vector<vector<int>> &ret, TreeNode *node, int i) {
    if (node) {
        if (ret.size() < i + 1)
            ret.push_back({ node->val });
        else
            ret[i].push_back(node->val);
        if (i % 2 == 0) {
            zigzagLevelOrder_recursive(ret, node->right, i+1);
            zigzagLevelOrder_recursive(ret, node->left, i+1);
        }
        else {
            zigzagLevelOrder_recursive(ret, node->left, i+1);
            zigzagLevelOrder_recursive(ret, node->right, i+1);
        }
    }
}

vector<vector<int> > zigzagLevelOrder(TreeNode *root) {
    vector<vector<int>> ret;
    zigzagLevelOrder_recursive(ret, root, 0);
    
    return ret;
}

int isBalanced_treeSize(TreeNode *node) {
    if (node == nullptr)
        return 0;
    int left = isBalanced_treeSize(node->left);
    int right = isBalanced_treeSize(node->right);
    return left > right ? left + 1 : right + 1;
}

bool isBalanced(TreeNode *root) {
    if (root == nullptr)
        return true;
    if (abs(isBalanced_treeSize(root->left) - isBalanced_treeSize(root->right)) > 1)
        return false;
    else
        return isBalanced(root->left) && isBalanced(root->right);
}

uint32_t reverseBits(uint32_t n) {
    uint32_t ret = 0;
    for (int i = 0; i < 32; ++i) {
        ret *= 2;
        ret += n % 2;
        n /= 2;
    }
    return ret;
}

void rotate(int nums[], int n, int k) {
    if (k < 1 || n < 1 || k % n == 0)
        return;
    k %= n;
    int temp = nums[0];
    
    if (n % k == 0) {
        for (int i = 0; i < k; ++i) {
            for (int j = i; j < n + k; j += k) {
                swap(temp, nums[j % n]);
            }
        }
    }
    else {
        for (int i = 0, j = k; i <= n; j += k, ++i) {
            swap(temp, nums[j % n]);
        }
    }
}

bool grayCode_backtrack(vector<int> &ret, vector<bool> &marked, int n) {
    // this implementation will overflow when n > 12
    if (ret.size() == (1<<n))
        return true;
    
    for (int i = 0; i < n; ++i) {
        if (!marked[ret.back() ^ (1 << i)]) {
            marked[ret.back() ^ (1 << i)] = true;
            ret.push_back(ret.back() ^ (1 << i));
            if (grayCode_backtrack(ret, marked, n))
                return true;
            marked[ret.back() ^ (1 << i)] = false;
            ret.pop_back();
        }
    }
    
    return false;
}

vector<int> grayCode(int n) {
    if (n < 1)
        return {};
    vector<int> ret = { 0 };
    vector<bool> marked(1<<n, false);
    marked[0] = true;
    grayCode_backtrack(ret, marked, n);
    return ret;
}

void sortColors(int A[], int n) {
    int count[] = {0, 0, 0};
    int *B = new int(sizeof(int) * n);
    for (int i = 0; i < n; ++i) {
        ++count[A[i]];
        B[i] = A[i];
    }
    for (int i = 1; i < 3; ++i) {
        count[i] += count[i-1];
    }
    
    for (int i = n-1; i >= 0; --i) {
        A[--count[B[i]]] = B[i];
    }
}

string simplifyPath(string path) {
    path += '/';
    const string dotdot("..");
    const string dot(".");
    string ret("/");
    string temp;
    deque<string> subpath;
    
    for (int i = 0; i < path.size(); ++i) {
        if (path[i] == '/') {
            if (!temp.empty() && temp != dotdot && temp != dot)
                subpath.push_back(std::move(temp));
            else if(temp == dotdot) {
                if (!subpath.empty())
                    subpath.pop_back();
            }
            temp.clear();
        }
        else
            temp += path[i];
    }
    while (!subpath.empty()) {
        ret += subpath.front();
        subpath.pop_front();
        if (!subpath.empty())
            ret += '/';
    }
    
    return ret;
}

void setZeroes(vector<vector<int> > &matrix) {
    if (matrix.size() < 1)
        return;
    if (matrix[0].size() < 1)
        return;
    vector<bool> rows(matrix.size(), false);
    vector<bool> columns(matrix[0].size(), false);
    for (int i = 0; i < matrix.size(); ++i) {
        for (int j = 0; j < matrix[i].size(); ++j) {
            if (matrix[i][j] == 0) {
                rows[i] = true;
                columns[j] = true;
            }
        }
    }
    for (int i = 0; i < rows.size(); ++i) {
        if (rows[i] == true) {
            for(auto &&elem : matrix[i])
                elem = 0;
        }
    }
    for (int i = 0; i < columns.size(); ++i) {
        if (columns[i] == true) {
            for(int j = 0; j < matrix.size(); ++j) {
                matrix[j][i] = 0;
            }
        }
    }
}

bool searchMatrix(vector<vector<int>> &matrix, int target) {
    if (target < matrix[0][0] || target > matrix.back().back())
        return false;
    auto i = matrix.begin();
    while (i != matrix.end() && (*i)[0] <= target)
        ++i;
    return binary_search((*(i-1)).begin(), (*(i-1)).end(), target);
}

int strStr(const char *haystack, const char *needle) {
    if (needle[0] == '\0')
        return 0;
    int haystack_len = (int)strlen(haystack);
    int needle_len = (int)strlen(needle);
    if (haystack_len < needle_len)
        return -1;

    KMP kmp(needle);
    return static_cast<int>(kmp.search(haystack));
}

int strStr2(const char *haystack, const char *needle) {
    if (needle[0] == '\0')
        return 0;
    int haystack_len = (int)strlen(haystack);
    int needle_len = (int)strlen(needle);
    if (haystack_len < needle_len)
        return -1;
    
    for (int i = 0; ; ++i) {
        for (int j = 0; ; ++j) {
            if (needle_len == j)
                return i;
            if (haystack_len == i + j) // loop reaches the end of haystack
                return -1;
            if (haystack[i + j] != needle[j])
                break;
        }
    }
    
    // return -1;
}

int strStr1(const char *haystack, const char *needle) {
    if (needle[0] == '\0')
        return 0;
    int haystack_len = (int)strlen(haystack);
    int needle_len = (int)strlen(needle);
    if (haystack_len < needle_len)
        return -1;
    
    int j, k;
    for (int i = 0; haystack[i]; ++i) {
        j = 0;
        k = i;
        while (needle[j] && needle[j] == haystack[k]) {
            ++j;
            ++k;
        }
        if (needle[j] == '\0')
            return k-j;
        if (haystack[k] == '\0')
            return -1;
    }
    return -1;
}

bool next_permutation(vector<int> &num) {
    if (num.size() < 2) {
        return true;
    }

    size_t last_ordered = LONG_MAX;
    for (size_t i = 0; i + 1 < num.size(); ++i) {
        if (num[i] < num[i + 1]) {
            last_ordered = i;
        }
    }
    if (last_ordered == LONG_MAX) {
        reverse(num.begin(), num.end());
        return true;
    }

    size_t position_to_swap;
    for (position_to_swap = last_ordered + 1; position_to_swap + 1 < num.size(); ++position_to_swap) {
        if (num[position_to_swap + 1] <= num[last_ordered]) {
            break;
        }
    }

    swap(num[position_to_swap], num[last_ordered]);
    reverse(num.begin() + last_ordered + 1, num.end());

    return false;
}

void nextPermutation(vector<int> &num) {
    if (num.size() < 2) {
        return;
    }

    size_t last_ordered = INT_MAX;
    for (size_t i = 0; i + 1 < num.size(); ++i) {
        if (num[i] < num[i + 1]) {
            last_ordered = i;
        }
    }
    if (last_ordered == LONG_MAX) {
        reverse(num.begin(), num.end());
        return;
    }

    size_t position_to_swap;
    for (position_to_swap = last_ordered + 1; position_to_swap + 1 < num.size(); ++position_to_swap) {
        if (num[position_to_swap + 1] <= num[last_ordered]) {
            break;
        }
    }

    swap(num[position_to_swap], num[last_ordered]);
    reverse(num.begin() + last_ordered + 1, num.end());
}

void nextPermutation1(vector<int> &num) {
    if (num.size() < 2)
        return;
    
    int i = static_cast<int>(num.size() - 1); // index of the last element
    int min_i = 0;
    
    for (; i > 0; i--) {
        if (num[i-1] >= num[i])
            continue;
        else
            break;
    }
    if (i == 0) { // last permutation
        reverse(num.begin(), num.end());
        return;
    }
    else {
        min_i = i;
        --i;
    }
    for (int j = min_i; j < num.size(); ++j) {
        if (num[j] < num[min_i] && num[j] > num[i])
            min_i = j;
    }
    swap(num[i], num[min_i]);
    sort(num.begin()+i+1, num.end());
}

int firstMissingPositive(int A[], int n) {
    if (n < 1)
        return 1;
    
    for (int i = 0; i < n; ++i) {
        while (A[i] >= 0 && A[i] < n && A[i] != A[A[i] - 1])
            swap(A[i], A[A[i] - 1]);
    }
    
    for (int i = 0; i < n; ++i) {
        if (A[i] != i + 1)
            return i + 1;
    }
    
    return n + 1;
}

void calculateMinimumHP_setMinimum(vector<vector<int>> &minHP, vector<vector<int>> &dungeon, int row, int column) {
    if (row != dungeon.size() - 1 && column != dungeon[0].size() - 1)
        minHP[row][column] = max(min(minHP[row+1][column], minHP[row][column+1])-dungeon[row][column], 1);
    else if (row != dungeon.size() - 1)
        minHP[row][column] = max(minHP[row+1][column]-dungeon[row][column], 1);
    else if (column != dungeon[0].size() - 1)
        minHP[row][column] = max(minHP[row][column+1]-dungeon[row][column], 1);
}

void calculateMinimumHP_recursive(vector<vector<int>> &min, vector<vector<int>> &dungeon, int row, int column) {
    if (row != dungeon.size() - 1 && column != dungeon[0].size() - 1) {
        if (min[row + 1][column] == INT_MAX)
            calculateMinimumHP_recursive(min, dungeon, row + 1, column);
        if (min[row][column + 1] == INT_MAX)
            calculateMinimumHP_recursive(min, dungeon, row, column + 1);
    }
    else if (row != dungeon.size() - 1) {
        if (min[row + 1][column] == INT_MAX)
            calculateMinimumHP_recursive(min, dungeon, row + 1, column);
    }
    else if (column != dungeon[0].size() - 1) {
        if (min[row][column + 1] == INT_MAX)
            calculateMinimumHP_recursive(min, dungeon, row, column + 1);
    }
    
    calculateMinimumHP_setMinimum(min, dungeon, row, column);
}

int calculateMinimumHP(vector<vector<int>> &dungeon) {
    if (dungeon.empty() || dungeon[0].empty())
        return 1;
    
    vector<vector<int>> min;
    for (int i = 0; i < dungeon.size(); ++i)
        min.emplace_back(dungeon[0].size(), INT_MAX);
    min.back().back() = max(-dungeon.back().back() + 1, 1);
    
    calculateMinimumHP_recursive(min, dungeon, 0, 0);
    
    return min[0][0];
}

void levelOrder_recursive(vector<vector<int>> &ret, TreeNode *root, int i) {
    if (root) {
        if (ret.size() < i + 1)
            ret.push_back({ root->val });
        else
            ret[i].push_back(root->val);
        levelOrder_recursive(ret, root->left, i+1);
        levelOrder_recursive(ret, root->right, i+1);
    }
}

vector<vector<int>> levelOrderBottom(TreeNode *root) {
    if (root == nullptr)
        return {};
    
    vector<vector<int>> ret;
    levelOrder_recursive(ret, root, 0);
    
    return vector<vector<int>> (ret.rbegin(), ret.rend());
}

vector<vector<int>> levelOrder(TreeNode *root) {
    if (root == nullptr)
        return {};
    
    vector<vector<int>> ret;
    levelOrder_recursive(ret, root, 0);
    
    return ret;
}

vector<vector<int>> levelOrder1(TreeNode *root) {
    if (root == nullptr)
        return {};
    
    vector<vector<int>> ret;
    vector<int> temp;
    queue<TreeNode *> this_level;
    queue<TreeNode *> next_level;
    TreeNode *iter = nullptr;
    this_level.push(root);
    
    while (!next_level.empty() || !this_level.empty()) {
        temp.clear();
        while (!this_level.empty()) {
            iter = this_level.front();
            this_level.pop();
            temp.push_back(iter->val);
            if (iter->left)
                next_level.push(iter->left);
            if (iter->right)
                next_level.push(iter->right);
        }
        ret.push_back(temp);
        swap(this_level, next_level);
    }
    return ret;
}

vector<string> split(const string &s, char delim) {
    vector<string> ret;
    stringstream ss(s);
    string temp;
    while (getline(ss, temp, delim)) {
        if (!temp.empty())
            ret.push_back(temp);
    }
    return ret;
}

int stringToInteger(const string &s) {
    int ret = 0;
    for (auto &&x: s) {
        ret *= 10;
        ret += static_cast<int>(x - '0');
    }
    
    return ret;
}

int compareVersion(string version1, string version2) {
    if (version1.empty()) {
        if (version2.empty())
            return 0;
        else
            return -1;
    }
    
    if (version2.empty()) {
        if (version1.empty())
            return 0;
        else
            return 1;
    }
    
    vector<int> list1;
    vector<int> list2;
    
    for (auto &&x : split(version1, '.')) {
        list1.push_back(stringToInteger(x));
    }
    
    for (auto &&x : split(version2, '.')) {
        list2.push_back(stringToInteger(x));
    }
    
    int i;
    for (i = 0; i < list1.size() && i < list2.size(); ++i) {
        if (list1[i] > list2[i])
            return 1;
        else if (list1[i] < list2 [i])
            return -1;
    }
    
    if (list1.size() > list2.size()) {
        while (i < list1.size()) {
            if (list1[i] != 0)
                return 1;
            ++i;
        }
    }
    
    if (list1.size() < list2.size()) {
        while (i < list2.size()) {
            if (list2[i] != 0)
                return -1;
            ++i;
        }
    }
    
    return 0;
}

int findPeakElement(const vector<int> &num) {
    if (num.size() < 2)
        return 0;
    if (num[0] > num[1])
        return 0;
    if (num[num.size()-1] > num[num.size()-2])
        return static_cast<int>(num.size()-1);
    
    for (int i = 0; i < num.size()-2; ++i) {
        if (num[i] < num[i+1])
            if (num[i+1] > num[i+2])
                return i+1;
    }
        
    return 0;
}

int majorityElement(vector<int> &num) {
    int current_candidate = num[0];
    int counter = 1;
    
    for (int i = 1; i < num.size(); ++i) {
        if (counter == 0) {
            current_candidate = num[i];
            counter = 1;
        }
        else {
            if (current_candidate == num[i])
                ++counter;
            else
                --counter;
        }
    }
    
    return current_candidate;
}

int majorityElement1(vector<int> &num) {
    unordered_map<int, int> appeared;
    for (auto &&x : num) {
        if (appeared.find(x) == appeared.end())
            appeared[x] = 1;
        else
            ++appeared[x];
    }
    
    int major = 0;
    int max = 0;
    for (auto beg = appeared.begin(); beg != appeared.end(); ++beg) {
        if (beg->second > max) {
            major = beg->first;
            max = beg->second;
        }
    }
    
    return major;
}

string largestNumber_intToString(int n) {
    string ret;
    static const char list[11] = "0123456789";
    
    while (n > 9) {
        ret = list[n % 10] + ret;
        n /= 10;
    }
    ret = list[n] + ret;
    
    return ret;
}

vector<string> largestNumber_convert(const vector<int> &num) {
    vector<string> ret;
    
    for (auto &&x : num)
        ret.push_back(largestNumber_intToString(x));
    
    return ret;
}

bool largerstNumber_compare(const string &lhs, const string &rhs) {
    if (lhs == rhs)
        return false;
    
    size_t l1 = lhs.size();
    size_t l2 = rhs.size();
    for (int i = 0; i < l1 + l2; ++i) {
        if (lhs[i % l1] > rhs[i % l2])
            return true;
        if (lhs[i % l1] < rhs[i % l2])
            return false;
    }
    
    return false;
};

string largestNumber(vector<int> &num) {
    if (num.size() < 1)
        return 0;
    
    vector<string> num_str = largestNumber_convert(num);
    string ret;
    
    sort(num_str.begin(), num_str.end(), largerstNumber_compare);
    if (num_str[0] == "0")
        return "0";
    
    for (auto &&x : num_str)
        ret += x;
    
    return ret;
}

int hashDNA(const string &s) {
    int ret = 0;
    for (auto &&x : s) {
        ret *= 4;
        switch (x) {
            case 'A':
                ret += 0;
                break;
            case 'T':
                ret += 1;
                break;
            case 'G':
                ret += 2;
                break;
            case 'C':
                ret += 3;
                break;
            default:
                return 0;
        }
    }
    
    return ret;
}

vector<string> findRepeatedDnaSequences(string s) {
    if (s.size() < 10)
        return {};
    
    vector<string> ret;
    unordered_map<int, int> appeared;
    for (int i = 0; i <= s.size() - 10; ++i) {
        if (appeared.find(hashDNA(s.substr(i, 10))) == appeared.end()) {
            appeared[hashDNA(s.substr(i, 10))] = 1;
        }
        else {
            if (appeared[hashDNA(s.substr(i, 10))] == 1) {
                ret.push_back(s.substr(i, 10));
                appeared[hashDNA(s.substr(i, 10))] = 2;
            }
        }
    }
    
    return ret;
}

int trailingZeroes(int n) {
    if (n < 0)
        n = -n;
    if (n < 5)
        return 0;
    
    int num_factor5 = 0;
    while (n >= 5) {
        num_factor5 += n / 5;
        n /= 5;
    }
    
    return num_factor5;
}

string mirror(const string &s) {
    string ret;
    for (auto start = s.crbegin(); start != s.crend(); ++start) {
        if (*start == '(')
            ret.push_back(')');
        else
            ret.push_back('(');
    }
    return ret;
}

int longestValidParentheses_solver(const string &s) {
    int left = 0;
    int right = 0;
    int longest = 0;
    
    for (auto &&x : s) {
        if (x == '(')
            ++left;
        else
            ++right;
        
        if (left == right) {
            if (longest < 2 * right)
                longest = 2 * right;
        }
        if (left < right) {
            left = right = 0;
        }
    }
    
    return longest;
}

int longestValidParentheses(string s) {
    int longest1 = longestValidParentheses_solver(s);
    int longest2 = longestValidParentheses_solver(mirror(s));
    
    return longest1 > longest2 ? longest1 : longest2;
}

string convertToTitle(int n) {
    if (n < 1)
        return "";
    string ret;
    char list[] = "ZABCDEFGHIJKLMNOPQRSTUVWXY";
    int temp = 0;
    
    while (n > 0) {
        temp = n % 26;
        ret = list[temp] + ret;
        if (n % 26 == 0)
            n = n / 26 - 1;
        else
            n /= 26;
    }
    return ret;
    
}

int titleToNumber(string s) {
    if (s.size() < 1)
        return 0;
    
    int ret = 0;
    for (int i = 0; i < s.size(); ++i) {
        ret += pow(26, i) * (s[s.size()-i-1] - 'A' + 1);
    }
    return ret;
}

int hammingWeight(uint32_t n) {
    int ret = 0;
    
    for (int i = 0; i < 32; ++i) {
        if (n % 2 == 1)
            ++ret;
        n /= 2;
    }
    
    return ret;
}

int longestValidParentheses1(string s) {
    if (s.empty())
        return 0;
    
    int left = 0;
    int right = 0;
    int longest = 0;
    int current = 0;
    size_t last_closed = 0;
    bool consecutive = true;
    
    for (size_t i = 0; i < s.size(); ++i) {
        if (s[i] == '(')
            ++left;
        else
            ++right;
        
        if (left < right) {
            left = 0;
            right = 0;
            consecutive = false;
            continue;
        }
        if (left == right && left != 0) {
            last_closed = i;
            if (consecutive == true) {
                current += left + right;
                if (longest < current)
                    longest = current;
            }
            else {
                current = left + right;
                consecutive = true;
                if (longest < current)
                    longest = current;
            }
            left = 0;
            right = 0;
        }
    }
    
    left = right = 0;
    current = 0;
    consecutive = true;
    
    for (size_t i = s.size() - 1; i > last_closed; --i) {
        if (s[i] == '(')
            ++left;
        else
            ++right;
        
        if (right < left) {
            left = 0;
            right = 0;
            consecutive = false;
            continue;
        }
        if (left == right && left != 0) {
            if (consecutive == true) {
                current += left + right;
                if (longest < current)
                    longest = current;
            }
            else {
                current = left + right;
                consecutive = true;
                if (longest < current)
                    longest = current;
            }
            left = 0;
            right = 0;
        }
    }
    
    return longest;
}

bool find(vector<vector<char>> &board, set<pair<int, int>> &path, const string &word, size_t l, size_t i, size_t j) {
    if (l == word.size())
        return true;
    
    if (i > 0 && path.find({i-1, j}) == path.end() && word[l] == board[i-1][j]) {
        path.insert({i-1, j});
        if (true == find(board, path, word, l+1, i-1, j))
            return true;
        path.erase(path.find({i-1, j}));
    }
    if (i < board.size()-1 && path.find({i+1, j}) == path.end() && word[l] == board[i+1][j]) {
        path.insert({i+1, j});
        if (true == find(board, path, word, l+1, i+1, j))
            return true;
        path.erase(path.find({i+1, j}));
    }
    if (j > 0 && path.find({i, j-1}) == path.end() && word[l] == board[i][j-1]) {
        path.insert({i, j-1});
        if (true == find(board, path, word, l+1, i, j-1))
            return true;
        path.erase(path.find({i, j-1}));
    }
    if (j < board[0].size()-1 && path.find({i, j+1}) == path.end() && word[l] == board[i][j+1]) {
        path.insert({i, j+1});
        if (true == find(board, path, word, l+1, i, j+1))
            return true;
        path.erase(path.find({i, j+1}));
    }
    
    return false;
}

bool exist(vector<vector<char>> &board, string word) {
    if (board.size() < 1 || board[0].size() < 1 || word.size() < 1)
        return false;
    
    // using a set to represent path
    set<pair<int, int>> path;
    
    for (size_t i = 0; i < board.size(); ++i) {
        for (size_t j = 0; j < board[0].size(); ++j) {
            if (board[i][j] == word[0]) {
                path.insert(pair<int, int>(i, j));
                if (true == find(board, path, word, 1, i, j))
                    return true;
                path.erase(path.find(pair<int, int>(i, j)));
            }
        }
    }
    return false;
}

void combine_backtrack(vector<vector<int>> &ret, vector<int> temp, int i, int n, int k) {
    if (k == 0) {
        ret.push_back(temp);
        return;
    }
    while (i <= n) {
        temp.push_back(i);
        combine_backtrack(ret, temp, ++i, n, k-1);
        temp.pop_back();
    }
}

vector<vector<int>> combine(int n, int k) {
    vector<vector<int>> ret;
    vector<int> temp;
    
    combine_backtrack(ret, temp, 1, n, k);
    return ret;
}

void dfs(UndirectedGraphNode *node, unordered_map<UndirectedGraphNode *, UndirectedGraphNode *> &marked) {
    for (auto &&x : node->neighbors) {
        if (marked.find(x) == marked.end()) {
            marked[x] = new UndirectedGraphNode(x->label);
            marked[node]->neighbors.push_back(marked[x]);
            dfs(x, marked);
        }
        else {
            marked[node]->neighbors.push_back(marked[x]);
        }
    }
}

UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
    if (node == nullptr)
        return node;
    unordered_map<UndirectedGraphNode *, UndirectedGraphNode *> marked;
    marked[node] = new UndirectedGraphNode(node->label);
    dfs(node, marked);
    return marked[node];
}

#pragma clang diagnostic pop