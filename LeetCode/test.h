//
//  test.h
//  LeetCode
//
//  Created by RogerChen on 3/12/15.
//  Copyright (c) 2015 RogerChen. All rights reserved.
//

#ifndef LeetCode_test_h
#define LeetCode_test_h

#include "solution.h"
#include <assert.h>
#include <iostream>
#include <algorithm>
#include <random>
using std::sort;
using std::cout;
using std::shuffle;
using std::mt19937_64;

bool LinkListEQ(ListNode *lhs, ListNode *rhs);
bool isDiff(int i);

void test_minCut();
bool test_largestDivisibleSubset_check(vector<int> &result);
void test_largestDivisibleSubset();
void test_numDecodings();
void test_fractionToDecimal();
void test_ladderLength();
void test_surroundedRegion();
void test_getPermutation();
void test_permuteUnique();
void test_maximumGap();
void test_NFA();
void test_strStr();
void test_myPow();
void test_double();
void test_maxPoints();
void test_evalRPN();
void test_sortList();
void test_isSymmetric();
void test_buildTree();
void test_combinationSum();
void test_combinationSum2();
void test_addBinary();
void test_countAndSay();
void test_rob();
void test_numIslands();
void test_solveSudoku();
void test_isValidSudoku();
void test_zigzagLevelOrder();
void test_maxPathSum();
void test_isBalanced();
void test_reverseBits();
void test_rotate();
void test_isDiff();
void test_grayCode();
void test_sortColor();
void test_simplifyPath();
void test_setZeroes();
void test_searchMatrix();
void test_nextPermutation();
void test_firstMissingPositive();
void test_calculateMinimumHP();
void test_levelOrderBottom();
void test_compareVersion();
void test_split();
void test_stringToInteger();
void test_levelOrder();
void test_peakElement();
void test_majorityElement();
void test_BSTIterator();
void test_largestNumber();
void test_largestNumber_intToString();
void test_largestNumber_compare();
void test_findRepeatedDnaSequences();
void test_trailingZeroes();
void test_convertToTitle();
void test_longestValidParentheses();
void test_hammingWeight();
void test_titleToNumber();
void test_wordSearch();
void test_combine();
void test_cloneGraph();
void test_all();
void test();

#endif
